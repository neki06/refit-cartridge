package refit.client;

import refit.communication.REFITClientEndpointWorker;
import refit.communication.REFITMessageFormat;
import refit.replica.REFITStage;


public abstract class REFITBenchmarkClientLibrary extends REFITStage implements REFITClientProxy {

	protected final short id;
	protected REFITClientEndpointWorker communication;

	
	public REFITBenchmarkClientLibrary(short id) {
		super(null, null);
		this.id = id;
		
	}

	public void init(REFITClientEndpointWorker communication)
	{
		this.communication = communication;
	}


	// ####################
	// # ABSTRACT METHODS #
	// ####################

	public abstract byte[] invoke(byte[] request) throws Exception;
	public abstract void invokeAsync(byte[] request);
	public abstract boolean isResultStable();
	public abstract byte[] getResult();
	public abstract REFITMessageFormat getMessageFormat();
	
}
