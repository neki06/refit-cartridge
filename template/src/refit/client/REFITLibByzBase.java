package refit.client;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction;
import refit.message.REFITInstruction.REFITInvocationInstruction;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITReply;

public abstract class REFITLibByzBase extends REFITBenchmarkClientLibrary
{

	protected final short clientID;
	protected final REFITMessageAuthentication[] auths;
	protected final REFITReplyCertificate certificate;

	protected short primaryID;

	private   final Semaphore stableReplyLock;
	private   long invocationTimeStamp;

	@Override
	public String toString()
	{
		return String.format("LIBBYZ[%03d]", clientID);
	}

	public REFITLibByzBase(short clientID)
	{
		super( clientID );

		this.clientID = clientID;
		this.auths = new REFITMessageAuthentication[ REFITConfig.TOTAL_NR_OF_REPLICAS ];
		for( short i=0; i<auths.length; i++ )
			auths[i] = REFITMessageAuthentication.createForClientConnection( clientID, clientID, i );
		this.certificate = new REFITReplyCertificate(auths);
		this.stableReplyLock = new Semaphore(0);
		this.invocationTimeStamp = -1L;
		this.primaryID = 0;
	}

	@Override
	public byte[] invoke(byte[] request) throws InterruptedException
	{
		// Insert invocation request
		invokeAsync(request);

		// Wait until reply is stable
		if( !REFITConfig.CLIENT_USE_REQUEST_TIMEOUT )
			stableReplyLock.acquire();
		else
		{
			while(true) {
				boolean success = stableReplyLock.tryAcquire(REFITConfig.CLIENT_REQUEST_TIMEOUT, TimeUnit.MILLISECONDS);
				if(success) break;

				// Retry
				REFITInstruction invocation = new REFITInvocationInstruction();
				insertMessage(invocation);
			}
		}

		// Return result
		return certificate.result.payload;
	}

	@Override
	public void invokeAsync(byte[] request)
	{
		REFITInstruction invocation = new REFITInvocationInstruction(request);
		insertMessage(invocation);
		invocationTimeStamp = System.currentTimeMillis();
	}

	@Override
	public boolean isResultStable()
	{
		// Check whether result is stable
		boolean resultStable = certificate.isStable();
		if(resultStable) return true;

		// Check for retry
		long now = System.currentTimeMillis();
		if((now - invocationTimeStamp) > REFITConfig.CLIENT_REQUEST_TIMEOUT) {
			// Retry
			REFITInstruction invocation = new REFITInvocationInstruction();
			insertMessage(invocation);
		}
		return resultStable;
	}

	@Override
	public byte[] getResult()
	{
		stableReplyLock.acquireUninterruptibly();
		return certificate.result.payload;
	}

	@Override
	protected void handleMessage(REFITMessage message)
	{
		switch(message.type) {
		case REPLY:
			if( handleReply((REFITReply) message) )
				stableReplyLock.release();
			break;
		case INSTRUCTION:
			handleInstruction((REFITInvocationInstruction) message);
			break;
		default:
			REFITLogger.logError(this, "drop message of unexpected type " + message.type);
		}
	}


	protected boolean handleReply(REFITReply reply)
	{
		// Ignore replies that arrive after the result has become stable
		if(certificate.isStable()) return false;

		// Add reply to certificate
		if( !REFITConfig.VERIFY_REPLIES )
			reply.markVerified( true );

		certificate.add(reply);
		if(!certificate.isStable()) return false;

		// Update primary id
		REFITReply result = certificate.result;
		primaryID = result.contactReplicaID;

		return true;
	}


	protected abstract void handleInstruction(REFITInvocationInstruction instruction);
}