package refit.client;

import refit.communication.REFITMessageFormat;
import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction.REFITInvocationInstruction;
import refit.message.REFITRequest;
import refit.message.REFITUniqueID;


public class REFITLibByz extends REFITLibByzBase {

	private final short replyReplicaID;
	private long nextSeqNr;
	private REFITRequest request;


	public REFITLibByz(short clientID) {
		super(clientID);
		this.replyReplicaID = (short) (clientID % REFITConfig.TOTAL_NR_OF_REPLICAS);

		this.nextSeqNr = 0L;
	}


	@Override
	public REFITMessageFormat getMessageFormat()
	{
		return new REFITMessageFormat();
	}


//	public void init() {
//		// Find out the initial sequence number for this client
//		try {
//			REFITRequest initRequest = prepareRequest(new byte[0]);
//			sendRequest(initRequest, true);
//
//			long invocationTime = System.currentTimeMillis();
//			for(int i = 0; i < REFITConfig.TOTAL_NR_OF_REPLICAS; i++) {
//				REFITReply reply = receiveReply(invocationTime + REFITConfig.CLIENT_REQUEST_TIMEOUT - System.currentTimeMillis());
//				if(reply == null) break;
//				if(!certificate.add(reply)) continue;
//				nextSeqNr = Math.max(nextSeqNr, ByteBuffer.wrap(reply.payload).getLong());
//			}
//		} catch(IOException ioe) {
//			ioe.printStackTrace();
//		}
//		nextSeqNr++;
//		if(REFITLogger.LOG_CLIENT) REFITLogger.logClient(this, "initial sequence number set to " + nextSeqNr);
//	}


	// #######################
	// # REPLICA INTERACTION #
	// #######################

	@Override
	protected void handleInstruction(REFITInvocationInstruction instruction) {
		if(instruction.request != null) {
			// Prepare request
			REFITUniqueID uid = new REFITUniqueID(clientID, nextSeqNr++);
			request = new REFITRequest(uid, clientID, instruction.request, replyReplicaID);
			certificate.init(uid);

			// Append MAC
			request.serializeMessage(REFITConfig.HMAC_REPLICACAST_SIZE);
			for( short i=0; i<auths.length; i++ )
				auths[i].appendMulticastMAC( i, request );

			// Send request to primary
			communication.sendToReplica(request, primaryID);
		} else {
			// Retry
			REFITLogger.logWarning(this, String.format("retry %s (%d replies received)", request, certificate.replies.getVoteCount()));
			request = new REFITRequest(request.uid, clientID, request.payload, (short) -1);
			request.markPanic();

			// Append MAC
			request.serializeMessage(REFITConfig.HMAC_REPLICACAST_SIZE);
			for( short i=0; i<auths.length; i++ )
				auths[i].appendMulticastMAC( i, request );

			// Send request to all replicas
			communication.sendToAllReplicas(request);
		}
		progress();
	}

}
