package refit.client;


public abstract class REFITBenchmarkClient extends Thread {

	protected final short id;
	protected final REFITClientProxy service;
	
	
	public REFITBenchmarkClient(short id, REFITClientProxy service) {
		this.id = id;
		this.service = service;
	}
	
	
	protected abstract void work() throws Exception;

	
	@Override
	public void run() {
		while(!isInterrupted()) {
			try {
				work();
			} catch(Exception e) {
				e.printStackTrace();
				break;
			}
		}
	}

}
