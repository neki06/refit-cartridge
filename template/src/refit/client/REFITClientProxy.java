package refit.client;

public interface REFITClientProxy
{
	byte[] invoke(byte[] request) throws Exception;
}
