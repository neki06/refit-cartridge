package refit.client;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import refit.util.REFITIntervalStatistics;
import refit.util.REFITStatisticsListener;
import refit.util.TimeLog;

public class REFITBenchmarkBase implements REFITStatisticsListener
{

	public static REFITIntervalStatistics statistics;
	protected final TimeLog timeLog;
	protected final short   id;
	protected final long    timelogresol;
	protected final String  resultpath;

	public REFITBenchmarkBase(short id, long timelogresol, String resultpath) {
		this.id           = id;
		this.timelogresol = timelogresol;
		this.resultpath   = resultpath;

		timeLog = new TimeLog( timelogresol );
		statistics = new REFITIntervalStatistics(1000, this, timeLog);
	}



	@Override
	public void statisticsIntervalResult(int resultIndex, int eventCount, float eventValueAverage, long eventValueMin,
			long eventValueMax)
	{
		System.out.println(String.format("%4d %6d %6.0f (%6d/%6d)", resultIndex, eventCount, eventValueAverage, eventValueMin, eventValueMax));
	}

	@Override
	public void statisticsProgressResult(int eventCount)
	{
		//		if(eventCount == 100) System.err.println("OK");
	}

	@Override
	public void statisticsOverallResult(int nrOfIntervalls, int eventCount, float eventCountAverage, float eventValueAverage)
	{
		System.out.println(String.format("END: %6d %6.0f %6.0f (%3d)", eventCount, eventCountAverage, eventValueAverage, nrOfIntervalls));
	}


	protected void runBenchmark(long duration, REFITBenchmarkClient[] clients)
	{
		// Start clients if the
		if(clients != null) {
			for(REFITBenchmarkClient client: clients)
				if( client!=null )
					client.start();
		}

		long logspan = 0;
		// Wait for benchmark end
		statistics.start();
		try
		{
			if( duration<10000 )
				Thread.sleep( duration );
			else
			{
				Thread.sleep( duration*6/10 );
				System.out.println( "Start logging..." );
				statistics.reset();
				long s = System.nanoTime();
				Thread.sleep( duration*3/10 );
				statistics.stopLogging();
				logspan = System.nanoTime() - s;
				System.out.println( "Stopped logging." );
				Thread.sleep( duration*1/10 );
			}
		}
		catch(InterruptedException e) {}
		statistics.end();

		if( duration>=10000 )
		{
			SummaryStatistics sum = timeLog.generateSummary();

			long rps = timeLog.getValueCount()/(logspan/1_000_000_000);
			System.out.println( "TimeLog cnt " + timeLog.getValueCount() + " min " + timeLog.getMinValue() + " max " + timeLog.getMaxValue() +
					" mean " + (timeLog.getValueSum() / timeLog.getValueCount()) + " bincnt " + timeLog.getBinCount() + " logspan " + (logspan/1000) +
					" rps " + rps);
			System.out.print( sum );

			Path res = FileSystems.getDefault().getPath( resultpath, "client" + id + ".log" );

			System.out.println( "Write results to " + res );

			try( BufferedWriter writer = Files.newBufferedWriter( res, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING ) )
			{
				writer.write( "cnt;sum;min;max;mean;approxstddev;time;rps" );
				writer.newLine();
				writer.write( String.format( "%d;%d;%d;%d;%d;%f;%d;%d", timeLog.getValueCount(), timeLog.getValueSum(), timeLog.getMinValue(),
						timeLog.getMaxValue(), timeLog.getValueSum() / timeLog.getValueCount(), sum.getStandardDeviation(), logspan/1000, rps ) );
				writer.newLine();

				writer.write( "binstart;binend;count" );
				writer.newLine();
				for( Map.Entry<Long, Integer> e : timeLog.condenseValues().entrySet() )
				{
					writer.write( String.format( "%d;%d;%d", e.getKey(), e.getKey()+timelogresol, e.getValue() ) );
					writer.newLine();
				}
			}
			catch( IOException e )
			{
				throw new IllegalStateException( e );
			}
		}

		System.exit(0);
    }

}