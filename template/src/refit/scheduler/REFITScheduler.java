package refit.scheduler;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.config.REFITSchedulerConfig;


public class REFITScheduler extends Thread {
	
	// #############################
	// # TABLE OF UNASSIGNED TASKS #
	// #############################

	public static final Map<REFITSchedulerTaskType, SortedMap<Short, Collection<REFITSchedulerTask>>> UNASSIGNED_TASKS =
			new HashMap<>();
	
	
	public static void registerTask(REFITSchedulerTask task) {
		SortedMap<Short, Collection<REFITSchedulerTask>> group = UNASSIGNED_TASKS.get(task.taskType);

		if(group == null) {
			group = new TreeMap<>();
			UNASSIGNED_TASKS.put( task.taskType, group );
		}

		Collection<REFITSchedulerTask> entry = group.get( task.group );

		if(entry == null) {
			entry = new LinkedList<REFITSchedulerTask>();
			group.put(task.group, entry);
		}
		entry.add(task);
	}
	
	public static boolean checkTaskAssignments() {
		return UNASSIGNED_TASKS.isEmpty();
	}
	
	
	// ####################
	// # SCHEDULER OBJECT #
	// ####################
	
	private static final int STATISTICS_INTERVAL = 10000;
	public static boolean IS_SINGLE_THREADED = false;
	
	private final List<REFITSchedulerTask> assignedTasks;
	private REFITSchedulerTask[] tasks;
	private final Selector selector;
	private boolean[] readyTasks;
	private final int[] affinity;
	
	
	public REFITScheduler(REFITSchedulerConfig conf) {
		super( conf.getName() );

		affinity = conf.getAffinity();

		// Create list of assigned tasks
		this.assignedTasks = new LinkedList<REFITSchedulerTask>();
		
		// Create selector
		Selector s = null;
		try { s = Selector.open(); } catch(IOException ioe) { ioe.printStackTrace(); }
		this.selector = s;
	}


	public int[] getAffinity()
	{
		return affinity;
	}

	
	// ###################
	// # TASK ASSIGNMENT #
	// ###################

//	public void assignTasks(REFITSchedulerTaskType taskType) {
//		Collection<REFITSchedulerTask> entry = UNASSIGNED_TASKS.remove(taskType);
//		if(entry == null) return;
//		for(REFITSchedulerTask task: entry) assignTask(task);
//	}
	
	public void assignTask(REFITSchedulerTask task) {
		task.setScheduler(this);
		assignedTasks.add(task);
	}

	public SelectionKey registerIOTask(REFITSchedulerTask task, SelectableChannel channel, int operations) throws IOException {
		return channel.register(selector, operations, task);
	}

	public Collection<REFITSchedulerTask> getAssignedTasks()
	{
		return assignedTasks;
	}

	
	// ##################
	// # TASK EXECUTION #
	// ##################

	public void notifyProgress(REFITSchedulerTask task) {
		readyTasks[task.id] = true;
		if(this == Thread.currentThread()) return;
		selector.wakeup();
	}

	public void init()
	{
		// Prepare tasks
		tasks = assignedTasks.toArray(new REFITSchedulerTask[0]);
		for(short i = 0; i < tasks.length; i++) tasks[i].setID(i);

		// Prepare ready flags
		readyTasks = new boolean[tasks.length];
		for(short i = 0; i < readyTasks.length; i++) readyTasks[i] = true;
	}
	
	@Override
	public void run() {
		if( REFITLogger.LOG_EVENT )
			REFITLogger.logEvent( this, "Started scheduler with thread ID " + REFITScheduling.getSystemThreadID() );

		if( affinity!=null )
			REFITScheduling.setThreadAffinity( affinity );

		for( REFITSchedulerTask task : tasks )
			task.initContext();

		if(REFITConfig.COLLECT_STAGE_STATISTICS) runDebug();
		else runDefault();
	}
	
	public void runDefault() {
		selector.wakeup();
		while(!isInterrupted()) {
			// Block until at least one task becomes runnable
			try {
				selector.select();
			} catch(IOException ioe) {
				ioe.printStackTrace();
				return;
			}
			
			// Collect tasks that are able to perform I/O operations
			for(SelectionKey key: selector.selectedKeys()) {
				REFITSchedulerTask task = (REFITSchedulerTask) key.attachment();
				task.selectKey(key);
				readyTasks[task.id] = true;
			}
			selector.selectedKeys().clear();
			
			// Handle runnable tasks
			while(!isInterrupted()) {
				// Execute runnable tasks
				boolean progress = false;
				for(int i = 0; i < tasks.length; i++) {
					if(!readyTasks[i]) continue;
					readyTasks[i] = false;
					progress = true;
					tasks[i].work();
				}
				
				// Break if none of the tasks has been executed in this loop
				if(!progress) break;
			}
		}
	}

	public void runDebug() {
		// Prepare statistics
		int epochCounter = 0;
		long epochStartTimestamp = System.currentTimeMillis();
		long selectDurations = 0L;
		long selectDurationsMax = 0L;
		int[] executions = new int[tasks.length];
		long[] executionDurations = new long[tasks.length];
		long[] executionDurationsMax = new long[tasks.length];
		long[] performances = new long[tasks.length];
		long[] throughputs = new long[tasks.length];

		selector.wakeup();
		while(!isInterrupted()) {
			// Start a new epoch
			epochCounter++;
			long selectDuration = -System.nanoTime();
			
			// Block until at least one task becomes runnable
			try {
				selector.select();
			} catch(IOException ioe) {
				ioe.printStackTrace();
				return;
			}
			
			// Collect tasks that are able to perform I/O operations
			for(SelectionKey key: selector.selectedKeys()) {
				REFITSchedulerTask task = (REFITSchedulerTask) key.attachment();
				task.selectKey(key);
				readyTasks[task.id] = true;
			}
			selector.selectedKeys().clear();

			// Collect select statistics
			selectDuration += System.nanoTime();
			selectDurations += selectDuration;
			selectDurationsMax = Math.max(selectDurationsMax, selectDuration);
			
			// Handle runnable tasks
			while(!isInterrupted()) {
				// Execute runnable tasks
				boolean progress = false;
				for(int i = 0; i < tasks.length; i++) {
					if(!readyTasks[i]) continue;
					readyTasks[i] = false;
					progress = true;
					
					// Execute task
					long duration = -System.nanoTime();
					tasks[i].work();
					duration += System.nanoTime();
					executions[i]++;
					executionDurations[i] += duration;
					executionDurationsMax[i] = Math.max(executionDurationsMax[i], duration);
					long throughput = tasks[i].collectThroughput();
					performances[i] += throughput;
					throughputs[i] += throughput;
				}
				
				// Break if none of the tasks has been executed in this loop
				if(!progress) break;
			}
			
			// Handle statistics
			if(System.currentTimeMillis() < (epochStartTimestamp + STATISTICS_INTERVAL)) continue;
			String s = String.format("[STAGE] Epoch %04d: %6.0f (%10d)\n", epochCounter, selectDurations / (float) epochCounter, selectDurationsMax);
			for(int i = 0; i < tasks.length; i++) {
				s += String.format(
						"[STAGE]  %-10s | %6d | %6.0f (%10d) | %12d %12d | \n",
						tasks[i],
						executions[i],
						executionDurations[i] / (float) executions[i],	
						executionDurationsMax[i],
						performances[i],
						throughputs[i]);
			}
			System.out.println(s);
			
			// Reset statistics
			epochCounter = 0;
			epochStartTimestamp = System.currentTimeMillis();
			selectDurations = 0L;
			selectDurationsMax = 0L;
			executions = new int[tasks.length];
			executionDurations = new long[tasks.length];
			executionDurationsMax = new long[tasks.length];
			performances = new long[tasks.length];
		}
	}

}
