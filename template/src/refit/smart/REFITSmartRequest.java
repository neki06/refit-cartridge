package refit.smart;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import refit.message.REFITMessageType;
import refit.message.REFITRequest;
import refit.message.REFITUniqueID;
import bftsmart.tom.core.messages.TOMMessage;

public class REFITSmartRequest extends REFITRequest
{
	private final TOMMessage tommsg;

	public REFITSmartRequest(TOMMessage tommsg)
	{
		super( REFITMessageType.REQUEST, new REFITUniqueID( (short) -1, tommsg.getSequence() ),
				(short) tommsg.getSender(), tommsg.getContent(), (short) -1 );

		this.tommsg = tommsg;
	}


	private REFITSmartRequest(REFITSmartRequest org)
	{
		super( org.type, org.uid, org.from, org.payload, org.replyReplicaID );

		tommsg = org.tommsg;
	}

	@Override
    public REFITSmartRequest clone()
	{
		REFITSmartRequest msg = new REFITSmartRequest( this );

		ByteBuffer buf = ByteBuffer.allocate( getMessageSize() );
		buf.put( getBuffer() );
		buf.rewind();

		msg.setSerialized( getMessageSize(), getPaddingSize(), buf );

		return msg;
	}


	@Override
	public boolean serializeMessage(int nrOfPaddingBytes)
	{
		if( hasSerialized() )
			return false;

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream     dos   = new DataOutputStream(baos);

		try
		{
			tommsg.wExternal(dos);
			dos.flush();
		}
		catch( IOException e )
		{
			throw new IllegalStateException( e );
		}

		byte[]     msgcnt = baos.toByteArray();
		ByteBuffer buf    = ByteBuffer.allocate( msgcnt.length + nrOfPaddingBytes );

		//-- In BFT-SMaRt, the header containing the length of the message is not counted in the message size.
        buf.put( msgcnt );

		setSerialized( buf.capacity(), nrOfPaddingBytes, buf );

		return true;
	}


	public TOMMessage getTOMMessage()
	{
		return tommsg;
	}
}
