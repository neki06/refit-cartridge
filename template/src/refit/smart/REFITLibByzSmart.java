package refit.smart;

import java.util.Arrays;

import refit.client.REFITLibByzBase;
import refit.communication.REFITMessageFormat;
import refit.config.REFITConfig;
import refit.message.REFITInstruction.REFITInvocationInstruction;
import bftsmart.tom.core.messages.TOMMessage;
import bftsmart.tom.core.messages.TOMMessageType;


public class REFITLibByzSmart extends REFITLibByzBase {

	private int nextSeqNr;

	public REFITLibByzSmart(short clientID) {
		super(clientID);

		this.nextSeqNr = 0;
	}


	@Override
	public REFITMessageFormat getMessageFormat()
	{
		return new REFITSmartMessageFormat();
	}


	// #######################
	// # REPLICA INTERACTION #
	// #######################

	@Override
	protected void handleInstruction(REFITInvocationInstruction instruction)
	{
		int seqnr = nextSeqNr++;

		TOMMessage        tommsg = new TOMMessage( clientID, 769569906, seqnr, seqnr, instruction.request, 0, TOMMessageType.ORDERED_REQUEST );
		REFITSmartRequest req    = new REFITSmartRequest( tommsg );
		certificate.init( req.uid );

		//-- Avoiding req.clone() is difficult. The REFITConnection does not have the necessary information about
		//-- messages to write only necessary parts of serialized messages to the send buffer.
		//-- It solely operates on buffers!
		req.serializeMessage( REFITConfig.HMAC_UNICAST_SIZE );

		for( short recipient=0; recipient<REFITConfig.TOTAL_NR_OF_REPLICAS; recipient++ )
		{
			if( recipient>0 )
				req = req.clone();

			auths[ recipient ].appendUnicastMAC( recipient, req );

			communication.sendToReplica( req, recipient );
		}
	}

}
