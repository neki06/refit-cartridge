package refit.replica.execution;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction;
import refit.message.REFITInstruction.REFIITConfigurationNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITRequest;
import refit.replica.REFITReplica;
import refit.replica.REFITStage;
import refit.replica.checkpoint.REFITCheckpoint;
import refit.replica.order.REFITOrderProtocol;
import refit.scheduler.REFITSchedulerTaskType;


public class REFITExecutionStage extends REFITStage {

	protected final int id;

	private final REFITExecutor executor;
	private final Map<Long, REFITOperation> requests;
	private long nextAgreementSeqNrToProcess;
	private final REFITMessageAuthentication auth;

	
	public REFITExecutionStage(int id, REFITReplica replica) {
		super(REFITSchedulerTaskType.EXECUTION_STAGE, replica);
		this.id = id;
		this.executor = new REFITExecutor(id, replica);
		this.nextAgreementSeqNrToProcess = id;
		this.checkpoints = new TreeMap<Long, REFITOperation>(OPERATION_PRIORITY_COMPARATOR);
		this.requests = new HashMap<Long, REFITOperation>();
		this.checkpointProgress = false;
		this.updateProgress = false;
		this.requestProgress = false;
		this.auth = REFITMessageAuthentication.createForReplicaConnection( replica.id );
	}
	
	
	@Override
	public String toString() {
		return String.format( "EXE%02d", id );
	}

	@Override
	protected void handleMessage(REFITMessage message) {
		switch(message.type) {
		case OPERATION:
			handleOperation((REFITOperation) message);
			break;
		case CHECKPOINT:
			handleCheckpoint((REFITCheckpoint) message);
			break;
		case INSTRUCTION:
			handleInstruction((REFITInstruction) message);
			break;
		default:
			REFITLogger.logError(this, "drop message of unexpected type " + message.type);
		}
	}

	private void handleOperation(REFITOperation operation) {
		// Ignore old operations
		if(operation.agreementSeqNr < nextAgreementSeqNrToProcess) return;
		
		// Store operation
		switch(operation.operationType) {
		case REQUEST:
			REFITOperation op = requests.get(operation.agreementSeqNr);
			if((op != null) && (op.isExecutable())) break;
			requests.put(operation.agreementSeqNr, operation);
			if((operation.agreementSeqNr == nextAgreementSeqNrToProcess) && operation.isExecutable()) requestProgress = true;
			break;
		case CHECKPOINT:
			op = checkpoints.get(operation.agreementSeqNr);
			if((op != null) && (op.isExecutable())) break;
			checkpoints.put(operation.agreementSeqNr, operation);
			if(operation.agreementSeqNr >= nextAgreementSeqNrToProcess) checkpointProgress = true;
			break;
		default:
			REFITLogger.logError(this, "drop operation of unexpected type " + operation.operationType);
		}
	}
	
	private void handleInstruction(REFITInstruction instruction) {
		switch(instruction.instructionType) {
		case CONFIGURATION_NOTIFICATION:
			REFIITConfigurationNotification notification = (REFIITConfigurationNotification) instruction;
			REFITOrderProtocol protocol = notification.protocol;
			executor.setEnableCheckpoints(protocol.hasCheckpointRecipients(), protocol.useLightweightCheckpoints());
			break;
		default:
			REFITLogger.logError(this, "drop instruction of unexpected type " + instruction.instructionType);
		}
	}
	
	
	// #######################
	// # PRIORITY COMPARATOR #
	// #######################

	private static final REFITExecutionStagePriorityComparator OPERATION_PRIORITY_COMPARATOR = new REFITExecutionStagePriorityComparator();
	
	
	private static class REFITExecutionStagePriorityComparator implements Comparator<Long> {

		@Override
		public int compare(Long agreementSeqNrA, Long agreementSeqNrB) {
			return (int) (agreementSeqNrB - agreementSeqNrA);
		}
		
	}
	
	
	// ###############
	// # CHECKPOINTS #
	// ###############

	private final Map<Long, REFITOperation> checkpoints;
	
	
	private void handleCheckpoint(REFITCheckpoint checkpoint) {
		// Ignore old and hash checkpoints they will not lead to progress
		if(checkpoint.agreementSeqNr < nextAgreementSeqNrToProcess) return;
		if(!checkpoint.isFullCheckpoint) return;
		
		// Get operation
		REFITOperation operation = checkpoints.get(checkpoint.agreementSeqNr);
		if(operation == null) {
			REFITLogger.logError(this, "no operation for full checkpoint");
			return;
		}
		
		// Make operation executable
		boolean success = operation.setFullMessage(checkpoint, auth);
		if(!success) {
			REFITLogger.logWarning(this, "full checkpoint did not lead to the corresponding operation becoming executable");
			return;
		}
		checkpointProgress = true;
	}
	
	private void garbageCollectCheckpoints() {
		for(Iterator<REFITOperation> iterator = checkpoints.values().iterator(); iterator.hasNext(); ) {
			REFITOperation checkpointOperation = iterator.next();
			if(checkpointOperation.agreementSeqNr < nextAgreementSeqNrToProcess) iterator.remove();
		}
	}

	
	// ############
	// # PROGRESS #
	// ############
	
	private boolean checkpointProgress;
	private boolean updateProgress;
	private boolean requestProgress;
	

	@Override
	protected void stageComplete() {
		boolean progress = false;
		if(checkpointProgress) {
			progress = processCheckpoints();
			// TODO Garbage collect updates and requests
		}
		
		if(updateProgress || requestProgress || progress) {
			processOperations();
			garbageCollectCheckpoints();
		}
		
		
		// Initiate message fetching
//		if(checkpointToFetch != null) {
//			
//		}
		
		// Reset flags
		checkpointProgress = false;
		updateProgress = false;
		requestProgress = false;
		
		// Perform garbage collection
//		if(progress) collectGarbage();
	}
	
	private boolean processCheckpoints() {
		// Check whether there is a stable checkpoint that allows the replica to make progress
		boolean progress = false;
		boolean delete = false;
		for(Iterator<REFITOperation> iterator = checkpoints.values().iterator(); iterator.hasNext(); ) {
			// Delete checkpoints with old sequence numbers
			REFITOperation checkpointOperation = iterator.next();
			if(delete || (checkpointOperation.agreementSeqNr < nextAgreementSeqNrToProcess)) {
				iterator.remove();
				delete = true;
				continue;
			}
			
			// Apply latest full stable checkpoint
			if(checkpointOperation.isExecutable()) {
				// Apply checkpoint
				REFITCheckpoint checkpoint = (REFITCheckpoint) checkpointOperation.message;
				executor.applyCheckpoint(checkpoint);
				nextAgreementSeqNrToProcess = checkpointOperation.agreementSeqNr + REFITConfig.EXECUTIONSTAGE.getNumber();
				progress = true;
				
				// Discard checkpoint and all others that are older
				iterator.remove();
				delete = true;
				continue;
			}
		}
		
		if(!progress) {
			System.out.println("TODO: implement checkpoint fetching"); // Somewhere above: Mark the latest stable checkpoint for transfer
			return progress;
		}
		
		// Garbage collect requests
		for(Iterator<Long> iterator = requests.keySet().iterator(); iterator.hasNext(); ) {
			if(iterator.next() < nextAgreementSeqNrToProcess) iterator.remove();
		}
		return progress;
	}

	private boolean processOperations() {
		// Check whether there are stable updates or requests that allow the replica to make progress
		boolean progress = false;
		while(true) {
			// Try to process a full stable request
			REFITOperation requestOperation = requests.get(nextAgreementSeqNrToProcess);
			if(requestOperation == null) break;
			
			// Process latest stable request
			if(requestOperation.isExecutable()) {
				requests.remove(nextAgreementSeqNrToProcess);
				executor.processRequest((REFITRequest) requestOperation.message, requestOperation.agreementSeqNr);
				nextAgreementSeqNrToProcess += REFITConfig.EXECUTIONSTAGE.getNumber();
				progress = true;
				continue;
			}
			
			// TODO: When and what to fetch
//			System.err.println("TODO: implement request/update fetching");
			break;
		}
		return progress;
	}

}
