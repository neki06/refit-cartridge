package refit.replica.execution;

import refit.application.counter.REFITCounterServer;
import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITBatch;
import refit.message.REFITInstruction.REFITPartialCheckpointNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageType;
import refit.message.REFITReply;
import refit.message.REFITRequest;
import refit.message.REFITUniqueID;
import refit.replica.REFITReplica;
import refit.replica.checkpoint.REFITCheckpoint;
import refit.replica.checkpoint.REFITCheckpointStage;
import refit.util.REFITMessageStatistics;
import refit.util.REFITPayload;


public class REFITExecutor {

	private final REFITReplica replica;
	private final REFITCounterServer application;
	private final int exectid;
	
	
	public REFITExecutor(int exectid, REFITReplica replica) {
		this.exectid = exectid;
		this.replica = replica;
		this.application = new REFITCounterServer( exectid, REFITConfig.EXECUTIONSTAGE.getNumber(), REFITConfig.TOTAL_NR_OF_REPLICAS,
				REFITConfig.TOTAL_NR_OF_CLIENTS, REFITConfig.REPLY_SIZE );
		this.enableCheckpoints = false;
		this.nodeProgresses = new long[REFITConfig.ADDRESSES.length];
		for(int i = 0; i < nodeProgresses.length; i++) nodeProgresses[i] = -1L;

		nchkptstages = REFITConfig.CHECKPOINTSTAGE.propagateToAll() ?
				REFITConfig.CHECKPOINTSTAGE.getNumber() :
				REFITConfig.EXECUTIONSTAGE.getCheckpointStageIDs( exectid ).length;
		combinedchkpts = REFITConfig.CHECKPOINTSTAGE.propagateToAll() && REFITConfig.EXECUTIONSTAGE.getNumber() > 1;
	}
	
	
	@Override
	public String toString() {
		return String.format( "EXR%02d", exectid );
	}
	
	
	// ##########################
	// # EXACTLY-ONCE EXECUTION #
	// ##########################
	
	private long[] nodeProgresses;

	
	private boolean ensureExactlyOnce(REFITUniqueID uid) {
		if(REFITRequest.NO_OP.uid.equals(uid)) return false;
		if(uid.seqNr <= nodeProgresses[uid.nodeID]) return false;
		nodeProgresses[uid.nodeID] = uid.seqNr;
		return true;
	}
	
	
	// ###############
	// # CHECKPOINTS #
	// ###############

	private boolean enableCheckpoints;
	private int     nextCheckpointStage = 0;
	private long    lastCheckpoint      = 0;

	private final int     nchkptstages;
	private final boolean combinedchkpts;

	
	public void setEnableCheckpoints(boolean enableCheckpoints, boolean createLightWeightCheckpoints) {
		REFITLogger.logEvent(this, "change checkpoint-creation setting to \"" + (enableCheckpoints ? (createLightWeightCheckpoints ? "lightweight" : "regular") : "disabled") + "\"");
		this.enableCheckpoints = enableCheckpoints;
	}
	
	public void applyCheckpoint(REFITCheckpoint checkpoint) {
		if(REFITLogger.LOG_EXECUTION) REFITLogger.logExecution(this, "apply checkpoint " + checkpoint);

		if( combinedchkpts )
			application.applyCheckpoint( REFITCheckpointStage.getStateForExecutor( exectid, checkpoint ) );
		else
			application.applyCheckpoint(checkpoint.payload);

		nodeProgresses = checkpoint.nodeProgresses;

		reachedCheckpoint( checkpoint.agreementSeqNr );
	}
	
	private void createCheckpoint(long agreementSeqNr) {
		if(REFITLogger.LOG_EXECUTION) REFITLogger.logExecution(this, "create checkpoint for agreement sequence number " + agreementSeqNr);

		byte[]       checkpointData = application.createCheckpoint();
		REFITMessage checkpoint = combinedchkpts ?
				new REFITPartialCheckpointNotification( exectid, agreementSeqNr, checkpointData, nodeProgresses.clone() ) :
				new REFITCheckpoint(replica.id, agreementSeqNr, checkpointData, nodeProgresses, true);
		replica.getCheckpointStageForInstance( agreementSeqNr ).insertMessage(checkpoint);
	}
	
	
	// ############
	// # REQUESTS #
	// ############
	
	public void processRequest(REFITRequest request, long agreementSeqNr) {
		// Debug output
		if(REFITLogger.LOG_EXECUTION) REFITLogger.logExecution(this, "process request " + request + " (" + agreementSeqNr + ")");
		if( REFITLogger.LOG_EVENT && (agreementSeqNr % 5000) == 0)
			REFITLogger.logEvent(this, String.format("%10d @ %13d", agreementSeqNr, System.currentTimeMillis()));

		// Execute request
		if(REFITConfig.ENABLE_BATCHING && (request.type == REFITMessageType.BATCH)) {
			for(REFITRequest req: ((REFITBatch) request).requests) processSingleRequest(req, agreementSeqNr);
		} else {
			processSingleRequest(request, agreementSeqNr);
		}

		long chkseqnr = combinedchkpts ? agreementSeqNr - exectid + nextCheckpointStage : agreementSeqNr;

		// Create checkpoint
		if(enableCheckpoints && (chkseqnr - lastCheckpoint) >= REFITConfig.CHECKPOINT_INTERVAL &&
				(chkseqnr % nchkptstages) == nextCheckpointStage )
		{
			createCheckpoint(chkseqnr);
			reachedCheckpoint(chkseqnr);
		}
	}

	private void reachedCheckpoint(long agreementSeqNr)
	{
		lastCheckpoint      = agreementSeqNr;
		nextCheckpointStage = (int) (agreementSeqNr + 1) % nchkptstages;
	}

	private void processSingleRequest(REFITRequest request, long agreementSeqNr) {
		// Check whether to process the request
		if(!ensureExactlyOnce(request.uid)) return;
		if(REFITConfig.COLLECT_MESSAGE_STATISTICS) REFITMessageStatistics.track(request, "PEX");

		// Process request
		byte[] result = application.processRequest( request.uid.nodeID, request.payload, agreementSeqNr );
		
		// Create reply
		REFITReply reply = new REFITReply(request.uid, replica.id, true, result);
		if(REFITConfig.USE_HASHED_REPLIES) {
			if((request.replyReplicaID >= 0) && (request.replyReplicaID != replica.id) && (reply.payload.length > REFITPayload.HASH_SIZE)) reply.sendHashReply();
		}
		if(REFITConfig.COLLECT_MESSAGE_STATISTICS) reply.getContext().statistics = request.context.statistics;

		replica.getClientStageForClient( request.from ).insertMessage(reply);
	}

}
