package refit.replica;

import javax.crypto.SecretKey;

import refit.config.REFITConfig;
import refit.message.REFITInstruction.REFITWorkNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;

public class REFITMulticastInstruction extends REFITWorkNotification
{
	private final REFITReplica replica;
	private final REFITMessage msg;
	private final int          salt;
	private final SecretKey    key;

	public REFITMulticastInstruction(REFITReplica replica, REFITMessage msg, int salt, SecretKey key)
	{
		this.replica = replica;
		this.msg     = msg;
		this.salt    = salt;
		this.key     = key;
	}


	@Override
	public void execute(REFITMessageAuthentication auth)
	{
		msg.serializeMessage( REFITConfig.HMAC_REPLICACAST_SIZE );

		auth.initKey( key );
		auth.appendMulticastMAC( msg );

		replica.replicaMulticast( msg, salt );
	}

}
