package refit.replica.checkpoint;

import java.util.List;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction.REFITPartialCheckpointNotification;
import refit.message.REFITMessageAuthentication;
import refit.util.REFITBallotBox;
import refit.util.REFITPayload;


public class REFITCheckpointCertificate {

	public final long agreementSeqNr;
	public final REFITBallotBox<Short, REFITCheckpointCertificateVote, REFITCheckpoint> checkpoints;
	public final REFITPartialCheckpointNotification partchks[];
	public int nr_part_chks;
	public int total_chkpt_size;

	private final REFITMessageAuthentication auth;


	public REFITCheckpointCertificate(long agreementSeqNr, REFITMessageAuthentication auth) {
		this.agreementSeqNr = agreementSeqNr;
		this.checkpoints = new REFITBallotBox<Short, REFITCheckpointCertificateVote, REFITCheckpoint>(REFITConfig.REGULAR_CHECKPOINT_STABILITY_THRESHOLD);
		this.auth = auth;

		partchks = REFITConfig.EXECUTIONSTAGE.getNumber() > 1 && REFITConfig.CHECKPOINTSTAGE.propagateToAll() ?
				new REFITPartialCheckpointNotification[ REFITConfig.EXECUTIONSTAGE.getNumber() ] : null;
	}

	public void add(REFITPartialCheckpointNotification partchk)
	{
		partchks[ partchk.exectid ] = partchk;
		total_chkpt_size += partchk.checkpointData.length;
		nr_part_chks++;
	}


	public boolean add(REFITCheckpoint checkpoint) {
		if(checkpoint == null) return false;
		if(agreementSeqNr != checkpoint.agreementSeqNr) return false;
		if(!auth.verifySignature(checkpoint)) return false;
		return checkpoints.add(checkpoint.from, new REFITCheckpointCertificateVote(checkpoint.getHash(), checkpoint.nodeProgresses), checkpoint);
	}

	public boolean isStable() {
		REFITCheckpointCertificateVote decision = checkpoints.getDecision();
		if(decision == null) return false;
		return true;
	}

	public REFITCheckpoint getStableCheckpoint() {
		// Check whether the checkpoint has become stable
		if(!isStable()) return null;
		
		// Return a full checkpoint if available
		List<REFITCheckpoint> correctCheckpoints = checkpoints.getDecidingBallots();
		for(REFITCheckpoint checkpoint: correctCheckpoints) {
			if(checkpoint.isFullCheckpoint) return checkpoint;
		}
		
		// No full checkpoint available -> Return a correct hash checkpoint
		return correctCheckpoints.get(0);
	}

	
	// ######################
	// # CHECKPOINT CONTENT #
	// ######################

	private static class REFITCheckpointCertificateVote {
		
		private final byte[] hash;
		private final long[] nodeProgress;
		
		
		public REFITCheckpointCertificateVote(byte[] hash, long[] nodeProgress) {
			this.hash = hash;
			this.nodeProgress = nodeProgress;
		}
		
		
		@Override
		public boolean equals(Object object) {
			if(REFITConfig.ENABLE_DEBUG_CHECKS) {
				if(object == null) return false;
				if(!(object instanceof REFITCheckpointCertificateVote)) return false;
			}
			REFITCheckpointCertificateVote other = (REFITCheckpointCertificateVote) object;
			if(!REFITPayload.matches(hash, other.hash)) return false;
			for(int i = 0; i < nodeProgress.length; i++) {
				if(nodeProgress[i] != other.nodeProgress[i]) {
					REFITLogger.logWarning("[CHKPT]", "client-progress mismatch @ " + i + ": " + nodeProgress[i] + " vs " + other.nodeProgress[i]);
					return false;
				}
			}
			return true;
		}
		
		@Override
		public int hashCode() {
			return hash.length;
		}
		
	}
	
}
