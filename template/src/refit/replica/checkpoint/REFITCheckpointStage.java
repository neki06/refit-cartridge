package refit.replica.checkpoint;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITInstruction;
import refit.message.REFITInstruction.REFIITConfigurationNotification;
import refit.message.REFITInstruction.REFITPartialCheckpointNotification;
import refit.message.REFITInstruction.REFITProgressNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.replica.REFITReplica;
import refit.replica.REFITStage;
import refit.replica.execution.REFITExecutionStage;
import refit.replica.execution.REFITOperation;
import refit.replica.execution.REFITOperation.REFITOperationType;
import refit.replica.order.REFITOrderStage;
import refit.scheduler.REFITSchedulerTaskType;
import refit.util.REFITPayload;


public class REFITCheckpointStage extends REFITStage {

	private final int id;
	private final REFITMessageAuthentication auth;

	public REFITCheckpointStage(int id, REFITReplica replica) {
		super(REFITSchedulerTaskType.CHECKPOINT_STAGE, replica);
		this.id = id;
		this.certificates = new HashMap<Long, REFITCheckpointCertificate>();
		this.auth = REFITMessageAuthentication.createForReplicaConnection( replica.id );
		this.latestStableCheckpoint = new REFITCheckpointCertificate(-1L, auth);
		this.checkpointRecipients = null;

		salt = hashCode();
	}

	
	@Override
	public String toString() {
		return String.format( "CHK%02d", id );
	}
	
	@Override
	protected void handleMessage(REFITMessage message) {
		switch(message.type) {
		case CHECKPOINT:
			handleCheckpoint((REFITCheckpoint) message);
			break;
		case INSTRUCTION:
			handleInstruction((REFITInstruction) message);
			break;
		default:
			REFITLogger.logError(this, "drop message of unexpected type " + message.type);
		}
	}

	private void handleInstruction(REFITInstruction instruction) {
		switch(instruction.instructionType) {
		case CONFIGURATION_NOTIFICATION:
			// Update the information on where to send checkpoints
			REFIITConfigurationNotification notification = (REFIITConfigurationNotification) instruction;
			checkpointRecipients = notification.protocol.getCheckpointRecipients();
			break;
		case PROGRESS_NOTIFICATION:
			REFITProgressNotification prognot = (REFITProgressNotification) instruction;
			handleStableCheckpoint( prognot.certificate );
			break;
		case PARTICAL_CHECKPOINT_NOTIFICATION:
			REFITPartialCheckpointNotification partchk = (REFITPartialCheckpointNotification) instruction;
			handlePartialCheckpoint( partchk );
			break;
		case PANIC_NOTIFICATION:
			// Mark all replicas as checkpoint recipients
			checkpointRecipients = new boolean[checkpointRecipients.length];
			for(int i = 0; i < checkpointRecipients.length; i++) checkpointRecipients[i] = true;
			
			// Distribute checkpoint messages that lead to the latest local stable checkpoint
			if(latestStableCheckpoint.agreementSeqNr >= 0) {
				if(REFITLogger.LOG_CHECKPOINT) REFITLogger.logCheckpoint(this, "distribute latest stable checkpoint certificate");
				for(REFITCheckpoint checkpoint: latestStableCheckpoint.checkpoints.getDecidingBallots()) {
					replica.replicaMulticast(checkpoint, salt++);
				}
			}
			break;
		default:
			REFITLogger.logError(this, "drop instruction of unexpected type " + instruction.instructionType);
		}
	}
	
	
	// #######################
	// # REGULAR CHECKPOINTS #
	// #######################

	private final Map<Long, REFITCheckpointCertificate> certificates;
	private REFITCheckpointCertificate latestStableCheckpoint;
	private boolean[] checkpointRecipients;
	private int       salt;
	
	
	private void handlePartialCheckpoint(REFITPartialCheckpointNotification partchk)
	{
		if( partchk.agreementSeqNr <= latestStableCheckpoint.agreementSeqNr )
			return;

		REFITCheckpointCertificate certificate = getCertificate( partchk.agreementSeqNr );
		certificate.add( partchk );

		if( certificate.nr_part_chks == REFITConfig.EXECUTIONSTAGE.getNumber() )
		{
			ByteBuffer buf = ByteBuffer.allocate( certificate.total_chkpt_size +
					(Integer.SIZE >> 3) * REFITConfig.EXECUTIONSTAGE.getNumber() );
			long[] nodeProgresses = new long[ REFITConfig.ADDRESSES.length ];

			for( REFITPartialCheckpointNotification p : certificate.partchks )
			{
				buf.putInt( p.checkpointData.length );
				buf.put( p.checkpointData );

				for( int i = REFITConfig.TOTAL_NR_OF_REPLICAS + p.exectid; i < nodeProgresses.length; i += REFITConfig.EXECUTIONSTAGE.getNumber() )
					nodeProgresses[ i ] = p.nodeProgresses[ i ];
			}

			handleCheckpoint( new REFITCheckpoint(replica.id, certificate.agreementSeqNr, buf.array(), nodeProgresses, true) );
		}
	}


	public static byte[] getStateForExecutor(int exectid, REFITCheckpoint combinedchk)
	{
		ByteBuffer buf = ByteBuffer.wrap( combinedchk.payload );

		while( exectid-- > 0 )
			buf.position( buf.getInt() + buf.position() );

		byte[] part = new byte[ buf.getInt() ];
		buf.get( part );

		return part;
	}

	private void handleCheckpoint(REFITCheckpoint checkpoint) {
		// Do not process old checkpoints
		if(checkpoint.agreementSeqNr <= latestStableCheckpoint.agreementSeqNr) {
			// Forward full checkpoint to execution stage, even if it is old, as the execution stage
			// currently might only have a checkpoint operation whose stability is based on hashes.
			// Verification of the old checkpoint will be done in the execution stage.
			if(checkpoint.isFullCheckpoint)
			{
				if( !REFITConfig.CHECKPOINTSTAGE.propagateToAll() )
					replica.executionStages[ REFITConfig.CHECKPOINTSTAGE.getExecutionStageID( id ) ].insertMessage( checkpoint );
				else
				{
					for( REFITExecutionStage es : replica.executionStages )
						es.insertMessage( checkpoint );
				}
			}

			return;
		}
		
		// Distribute own checkpoint
		if(checkpoint.from == replica.id) {
			REFITCheckpoint ownCheckpoint = REFITConfig.DISTRIBUTE_CHECKPOINT_HASHES ?
					new REFITCheckpoint(checkpoint.from, checkpoint.agreementSeqNr, checkpoint.getHash(), checkpoint.nodeProgresses, false) :
					checkpoint;
			ownCheckpoint.serializeMessage(REFITConfig.SIGNATURE_SIZE);
			auth.appendSignature(ownCheckpoint);
			if(REFITLogger.LOG_CHECKPOINT) REFITLogger.logCheckpoint(this, "distribute own checkpoint " + ownCheckpoint);
			replica.replicaMulticast(ownCheckpoint, checkpointRecipients, salt++);
		}

		// Only store checkpoint if the local replica is a checkpoint recipient
		if(!checkpointRecipients[replica.id]) return;

		// Get checkpoint certificate
		REFITCheckpointCertificate certificate = getCertificate( checkpoint.agreementSeqNr );
		
		// Store checkpoint
		if(REFITLogger.LOG_CHECKPOINT) REFITLogger.logCheckpoint(this, "received checkpoint " + checkpoint);
		boolean success = certificate.add(checkpoint);
		if(!success) {
			REFITLogger.logWarning(this, "bad CHECKPOINT (" + checkpoint + ")");
			return;
		}

		// Check whether the checkpoint became stable
		if(!certificate.isStable()) return;

		// Stable checkpoint reached
		handleStableCheckpoint(certificate);

		// Forward information that the checkpoint has become stable
		forwardStableCheckpoint(certificate.getStableCheckpoint(), certificate);
	}

	private REFITCheckpointCertificate getCertificate(long agreementSeqNr)
	{
		REFITCheckpointCertificate certificate = certificates.get(agreementSeqNr);
		if(certificate == null) {
			certificate = new REFITCheckpointCertificate(agreementSeqNr, auth);
			certificates.put(agreementSeqNr, certificate);
		}
		return certificate;
	}

	private void handleStableCheckpoint(REFITCheckpointCertificate certificate) {
		latestStableCheckpoint = certificate;
		certificates.remove(latestStableCheckpoint.agreementSeqNr);
		if(REFITLogger.LOG_CHECKPOINT) REFITLogger.logCheckpoint(this, "stable checkpoint for agreement sequence number " + latestStableCheckpoint.agreementSeqNr);
		
		// Garbage-collect older certificates
		for(Iterator<Map.Entry<Long, REFITCheckpointCertificate>> iterator = certificates.entrySet().iterator(); iterator.hasNext(); ) {
			Map.Entry<Long, REFITCheckpointCertificate> entry = iterator.next();
			if(entry.getKey() <= latestStableCheckpoint.agreementSeqNr) iterator.remove();
		}
	}
	
	private void forwardStableCheckpoint(REFITCheckpoint stableCheckpoint, REFITCheckpointCertificate certificate) {
		// Distribute progress notification
		REFITProgressNotification progressNotification = new REFITProgressNotification(stableCheckpoint.agreementSeqNr, stableCheckpoint.nodeProgresses, certificate);

		// Create and forward checkpoint operation
		REFITOperation operation;
		if(stableCheckpoint.isFullCheckpoint) {
			// Create checkpoint operation with full stable checkpoint
			operation = new REFITOperation(REFITOperationType.CHECKPOINT, stableCheckpoint.agreementSeqNr, stableCheckpoint);
		} else {
			// Create checkpoint operation with stable checkpoint hash
			operation = new REFITOperation(REFITOperationType.CHECKPOINT, stableCheckpoint.agreementSeqNr, stableCheckpoint.uid, new REFITPayload(stableCheckpoint.getHash()));
		}

		if( !REFITConfig.CHECKPOINTSTAGE.propagateToAll() )
		{
			replica.getOrderStageForInstance( stableCheckpoint.agreementSeqNr ).insertMessage( progressNotification );
			replica.executionStages[ REFITConfig.CHECKPOINTSTAGE.getExecutionStageID( id ) ].insertMessage(operation);
		}
		else
		{
			for( REFITOrderStage os : replica.orderStages )
				os.insertMessage(progressNotification);

			for( REFITCheckpointStage ks : replica.checkpointStages )
				if( ks != this )
					ks.insertMessage(progressNotification);

			for( REFITExecutionStage es : replica.executionStages )
				es.insertMessage( operation );
		}
	}

}
