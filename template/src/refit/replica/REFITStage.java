package refit.replica;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import refit.message.REFITMessage;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITSchedulerTask;
import refit.scheduler.REFITSchedulerTaskType;


public abstract class REFITStage extends REFITSchedulerTask {

	protected final REFITReplica replica;
	protected final Queue<REFITMessage> incomingMessages;
	
	
	public REFITStage(REFITSchedulerTaskType taskType, REFITReplica replica) {
		super(taskType, (short) 0);
		this.replica = replica;
		this.incomingMessages = REFITScheduler.IS_SINGLE_THREADED ? new LinkedList<REFITMessage>() : new ConcurrentLinkedQueue<REFITMessage>();
	}
	
	
	// ##################
	// # SCHEDULER TASK #
	// ##################

	@Override
	public void execute() {
		do {
			// Process messages
			REFITMessage message;
			while((message = incomingMessages.poll()) != null) {
				handleMessage(message);
				event();
			}
			
			// Complete stage (This method is allowed to insert additional messages into the queue.) 
			stageComplete();
		} while(!incomingMessages.isEmpty());
	}

	
	// #################
	// # STAGE METHODS #
	// #################

	public void insertMessage(REFITMessage message) {
		incomingMessages.add(message);
		progress();
	}

	protected abstract void handleMessage(REFITMessage message);

	protected void stageComplete() {
		// Override in sub classes if necessary
	}

}
