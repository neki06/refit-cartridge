package refit.replica;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

import refit.communication.REFITClientNetworkEndpoint;
import refit.communication.REFITReplicaNetworkEndpoint;
import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.config.REFITSchedulingConfig;
import refit.message.REFITInstruction.REFITProgressNotification;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITReply;
import refit.replica.checkpoint.REFITCheckpoint;
import refit.replica.checkpoint.REFITCheckpointStage;
import refit.replica.client.REFITClientStage;
import refit.replica.execution.REFITExecutionStage;
import refit.replica.order.REFITOrderProtocolMessage;
import refit.replica.order.REFITOrderStage;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITSchedulerTask;
import refit.scheduler.REFITSchedulerTaskType;
import refit.scheduler.REFITScheduling;



public class REFITReplica {

	public static REFITReplica instance;
	public final short id;
	
	public final REFITClientStage[] clientStages;
	public final REFITOrderStage[] orderStages;
	public final REFITCheckpointStage[] checkpointStages;
	public final REFITExecutionStage[] executionStages;
	public final REFITWorkerStage[] workerStages;
	
	
	public REFITReplica(short id) {
		// Configure replica
		instance = this;
		this.id = id;

		if( REFITLogger.LOG_INFO )
		{
			REFITLogger.logInfo( "CONFIG", "Batch size: " + REFITConfig.MINIMUM_BATCH_SIZE + " - " + REFITConfig.MAXIMUM_BATCH_SIZE );
			REFITLogger.logInfo( "CONFIG", "Checkpoint interval: " + REFITConfig.CHECKPOINT_INTERVAL );
			REFITLogger.logInfo( "CONFIG", "Propagate checkpoints to all: " + REFITConfig.CHECKPOINTSTAGE.propagateToAll() );
			REFITLogger.logInfo( "CONFIG", "Instance window: " + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_PER_STAGE + " x " +
					REFITConfig.ORDERSTAGE.getNumber() + " = " + REFITConfig.MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL );
			REFITLogger.logInfo( "CONFIG", "Send buffer: " + REFITConfig.REPLICA_NETWORK_SEND_BUFFER_SIZE );
			REFITLogger.logInfo( "CONFIG", "Recv buffer: " + REFITConfig.REPLICA_NETWORK_RECEIVE_BUFFER_SIZE );
			REFITLogger.logInfo( "CONFIG", "Client recv buffer: " + REFITConfig.CLIENT_NETWORK_RECEIVE_BUFFER_SIZE );
			REFITLogger.logInfo( "CONFIG", "Worker verification: " + REFITConfig.WORKERSTAGE.useForVerification() );
		}

		// Create client and replica networks
		this.clientNetwork = new REFITClientNetworkEndpoint(this);
		this.replicaNetwork = new REFITReplicaNetworkEndpoint(this);

		// Create stages
		this.workerStages = new REFITWorkerStage[ REFITConfig.WORKERSTAGE.getNumber() ];

		for( int i = 0; i < workerStages.length; i++ )
			workerStages[ i ] = new REFITWorkerStage( i, this );

		this.executionStages = new REFITExecutionStage[ REFITConfig.EXECUTIONSTAGE.getNumber() ];

		for( int i = 0; i < executionStages.length; i++ )
			executionStages[ i ] = new REFITExecutionStage( i, this );

		stageNetworks = new HashMap<>();

		checkpointStages = new REFITCheckpointStage[ REFITConfig.CHECKPOINTSTAGE.getNumber() ];

		for( int i = 0; i < checkpointStages.length; i++ )
		{
			checkpointStages[i] = new REFITCheckpointStage(i, this);
			stageNetworks.put( checkpointStages[i], REFITConfig.CHECKPOINTSTAGE.getNetworkIDs( i ) );
			if( REFITLogger.LOG_INFO )
			{
				REFITLogger.logInfo( "CONFIG", "Link stage " + checkpointStages[i] + " with nets " + Arrays.toString( REFITConfig.CHECKPOINTSTAGE.getNetworkIDs( i ) ) );
				REFITLogger.logInfo( "CONFIG", "Link stage " + checkpointStages[i] + " with " + executionStages[ REFITConfig.CHECKPOINTSTAGE.getExecutionStageID( i ) ] );
			}
		}

		orderStages = new REFITOrderStage[ REFITConfig.ORDERSTAGE.getNumber() ];

		for( int i = 0; i < orderStages.length; i++ )
		{
			orderStages[i] = new REFITOrderStage(i, this, REFITConfig.INITIAL_ORDER_PROTOCOL);
			stageNetworks.put( orderStages[i], REFITConfig.ORDERSTAGE.getNetworkIDs( i ) );
			if( REFITLogger.LOG_INFO )
			{
				REFITLogger.logInfo( "CONFIG", "Link stage " + orderStages[i] + " with nets " + Arrays.toString( REFITConfig.ORDERSTAGE.getNetworkIDs( i ) ) );
				REFITLogger.logInfo( "CONFIG", "Link stage " + orderStages[i] + " with " + executionStages[ REFITConfig.ORDERSTAGE.getExecutionStageID( i ) ] );
				REFITLogger.logInfo( "CONFIG", "Link stage " + orderStages[i] + " with workers " + Arrays.toString( REFITConfig.ORDERSTAGE.getWorkerIDs( i ) ) );
			}
		}

		clientStages = new REFITClientStage[ REFITConfig.CLIENTSTAGE.getNumber() ];
		for( short i = 0; i < clientStages.length; i++ )
		{
			clientStages[i] = new REFITClientStage(i, this);
			if( REFITLogger.LOG_INFO )
				for( int o : REFITConfig.CLIENTSTAGE.getOrderStageIDs( i ) )
				{
					REFITLogger.logInfo( "CONFIG", "Link stage " + clientStages[i] + " with " + orderStages[o] );
					REFITLogger.logInfo( "CONFIG", "Link stage " + clientStages[i] + " with workers " + Arrays.toString( REFITConfig.ORDERSTAGE.getWorkerIDs( i ) ) );
				}
		}
	}
	
	
	public void init() {
		// Initialize order stage
		REFITProgressNotification initProgressNotification = new REFITProgressNotification(-1L, null, null);

		for( REFITOrderStage os : orderStages )
		{
			os.insertMessage(initProgressNotification);
			os.learnViewChange(0, REFITConfig.INITIAL_ORDER_PROTOCOL);
		}
		
		// Initialize networks (Do not change order!)
		replicaNetwork.init();
		clientNetwork.init();
	}

	@Override
	public String toString() {
		return "RPLCA";
	}

	
	// ###########################
	// # CLIENT NETWORK ENDPOINT #
	// ###########################

	private final REFITClientNetworkEndpoint clientNetwork;

	public REFITClientStage getClientStageForClient(short clientID)
	{
		return clientStages[ clientID % REFITConfig.CLIENTSTAGE.getNumber() ];
	}

	public void receiveMessageFromClient(REFITMessage message) {
		getClientStageForClient( message.from ).insertMessage(message);
	}

	public void sendMessageToClient(REFITReply reply, short clientID) {
		clientNetwork.sendReply(reply, clientID);
	}


	// ############################
	// # REPLICA NETWORK ENDPOINT #
	// ############################
	
	private final REFITReplicaNetworkEndpoint replicaNetwork;
	private final Map<REFITStage, int[]> stageNetworks;
	
	
	public REFITOrderStage getOrderStageForInstance(long instanceID)
	{
		return orderStages[ (int) (instanceID % REFITConfig.ORDERSTAGE.getNumber()) ];
	}

	public REFITCheckpointStage getCheckpointStageForInstance(long instanceID)
	{
		return checkpointStages[ (int) (instanceID % REFITConfig.CHECKPOINTSTAGE.getNumber()) ];
	}

	public void receiveMessageFromReplica(REFITMessage message) {
		switch(message.type) {
		case ORDER:
			getOrderStageForInstance( ((REFITOrderProtocolMessage) message).instanceID ).insertMessage(message);
			break;
		case CHECKPOINT:
			getCheckpointStageForInstance( ((REFITCheckpoint) message).agreementSeqNr ).insertMessage(message);
			break;
		case REQUEST:
			// TODO: Forwarded requests, for instance in cases of PANIC.
			// Could be routed over client stage to obtain the associated order stage.
			throw new UnsupportedOperationException( message.toString() );
//			orderStage.insertMessage(message);
//			break;
		default:
			REFITLogger.logError(this, "drop message of unexpected type " + message.type);
		}
	}

	private int getNetwork(REFITMessage message, int salt)
	{
		REFITStage stage;

		switch(message.type) {
		case ORDER:
			stage = getOrderStageForInstance( ((REFITOrderProtocolMessage) message).instanceID );
			break;
		case CHECKPOINT:
			stage = getCheckpointStageForInstance( ((REFITCheckpoint) message).agreementSeqNr );
			break;
		case REQUEST:
			throw new UnsupportedOperationException( message.toString() );
		default:
			throw new IllegalStateException( message.toString() );
		}

		int[] nets = stageNetworks.get( stage );

		return nets[ Math.abs( salt % nets.length ) ];
	}

	public void replicaUnicast(REFITMessage message, short replicaID, int salt) {
		replicaNetwork.replicaUnicast( getNetwork(message, salt), message, replicaID );
	}

	public void replicaMulticast(REFITMessage message, int salt) {
		replicaNetwork.replicaMulticast( getNetwork(message, salt), message );
	}

	/* Value in replicas[id] will be ignored. */
	public void replicaMulticast(REFITMessage message, boolean[] replicas, int salt) {
		if(REFITConfig.ENABLE_DEBUG_CHECKS) if(replicas == null) REFITLogger.logDebug(this, "replicaMulticast(): replicas is not supposed to be null (" + message + ")");
		replicaNetwork.replicaMulticast( getNetwork(message, salt), message, replicas );
	}

	
	// ################
	// # TASK MAPPING #
	// ################

	public void start() {
		// Create schedulers
		REFITSchedulingConfig schedconf  = REFITConfig.REPLICA_SCHEDULING;
		REFITScheduler[]      schedulers = new REFITScheduler[schedconf.getSchedulers().length];

		for(int i = 0; i < schedulers.length; i++)
			schedulers[i] = new REFITScheduler( schedconf.getSchedulers()[i] );

		if(schedulers.length == 1) REFITScheduler.IS_SINGLE_THREADED = true;

		for( Map.Entry<REFITSchedulerTaskType, int[]> tt : schedconf.getGroupAssignment().entrySet() )
		{
			SortedMap<Short, Collection<REFITSchedulerTask>> groups = REFITScheduler.UNASSIGNED_TASKS.remove( tt.getKey() );
			int[] sids = tt.getValue();

			int i = 0;
			for( Collection<REFITSchedulerTask> tasks : groups.values() )
			{
				for( REFITSchedulerTask t : tasks )
					schedulers[ sids[ i ] ].assignTask( t );

				i = ++i % sids.length;
			}
		}

		for( Map.Entry<REFITSchedulerTaskType, int[]> tt : schedconf.getTaskAssignment().entrySet() )
		{
			SortedMap<Short, Collection<REFITSchedulerTask>> groups = REFITScheduler.UNASSIGNED_TASKS.remove( tt.getKey() );
			int[] sids = tt.getValue();

			int i = 0;
			for( Collection<REFITSchedulerTask> tasks : groups.values() )
				for( REFITSchedulerTask t : tasks )
					schedulers[ sids[ (i++ % sids.length) ] ].assignTask( t );
		}

		// Make sure all tasks have been assigned
		if(!REFITScheduler.checkTaskAssignments()) {
			REFITLogger.logError(this, "Not all tasks have been assigned to a scheduler");
			return;
		}
		
		// Start schedulers
		for(REFITScheduler scheduler: schedulers) {
			scheduler.init();
			scheduler.start();
		}

		REFITConfig.printSchedulingConfig( schedconf, schedulers );
	}

	
	// ########
	// # MAIN #
	// ########

	public static void main(String[] args) throws IOException {
		// Create and initialize replica
		short  replicaID = Short.parseShort(args[0]);
		String cf        = args[1];

		REFITConfig.load( cf );

		if( REFITConfig.REPLICA_SCHEDULING.getProcessAffinity()!=null )
			REFITScheduling.setProcessAffinity( REFITConfig.REPLICA_SCHEDULING.getProcessAffinity() );

		REFITReplica replica = new REFITReplica(replicaID);
		replica.init();
		
		// Start replica
		replica.start();

		// Print welcome
		String welcome = "READY";
		System.out.println(welcome);
	}

}
