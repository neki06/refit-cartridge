package refit.application.counter;

import java.nio.ByteBuffer;

import refit.client.REFITBenchmarkBase;
import refit.client.REFITBenchmarkClient;
import refit.client.REFITClientProxy;
import refit.config.REFITConfig;


public class REFITCounterClient extends REFITBenchmarkClient {

	private final byte[] request;
	private int invocationCounter;
	
	
	public REFITCounterClient(short id, REFITClientProxy service) {
		super(id, service);
		this.request = new byte[REFITConfig.REQUEST_SIZE];
		this.invocationCounter = 0;
	}

	
	@Override
	protected void work() throws Exception {
		// Invoke operation
		long startTime = System.nanoTime();
		byte[] result = service.invoke(request);
		long endTime = System.nanoTime();
		REFITBenchmarkBase.statistics.event((long) ((endTime - startTime) / 1000f));
		
		// Check result
		ByteBuffer resultBuffer = ByteBuffer.wrap(result);
		int value = resultBuffer.getInt();
		invocationCounter++;
		if(value != invocationCounter) throw new Exception("Bad result: counter value is " + value + " instead of " + invocationCounter);
	}

}
