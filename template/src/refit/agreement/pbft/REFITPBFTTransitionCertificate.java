package refit.agreement.pbft;

import java.util.Collection;
import java.util.LinkedList;

import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTCommit;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTMessage;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrePrepare;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrepare;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTViewChange;
import refit.config.REFITLogger;
import refit.message.REFITMessage;
import refit.message.REFITRequest;
import refit.replica.order.REFITOrderStageSlot;


public class REFITPBFTTransitionCertificate {

	private final int decisionThreshold;
	private final REFITOrderStageSlot slot;
	public final Collection<REFITPBFTViewChange> proof;
	public REFITMessage decision;
	
	
	public REFITPBFTTransitionCertificate(int decisionThreshold, REFITOrderStageSlot slot) {
		this.decisionThreshold = decisionThreshold;
		this.proof = new LinkedList<REFITPBFTViewChange>();
		this.decision = REFITRequest.NO_OP;
		this.slot = slot;
	}
	

	@Override
	public String toString() {
		return "TCERT";
	}
	

	// ###################
	// # ADDING MESSAGES #
	// ###################

	public boolean addViewChange(REFITPBFTViewChange viewChange, boolean isLocalMessage) {
		// Check VIEW_CHANGE
		if(viewChange == null) return false;
		if(!slot.getOrderStage().getAuth().verifySignature(viewChange)) return false;
		
		// Check VIEW_CHANGE content
		if(isLocalMessage) {
			decision = viewChange.message;
		} else if(viewChange.message != REFITRequest.NO_OP) {
			// Recreate agreement process
			REFITPBFTCertificate certificate = new REFITPBFTCertificate(slot);
			for(REFITPBFTMessage pbftMessage: viewChange.proof) {
				switch(pbftMessage.pbftType) {
				case PBFT_PRE_PREPARE:
					certificate.addPrePrepare((REFITPBFTPrePrepare) pbftMessage);
					break;
				case PBFT_PREPARE:
					certificate.addPrepare((REFITPBFTPrepare) pbftMessage);
					break;
				case PBFT_COMMIT:
					certificate.addCommit((REFITPBFTCommit) pbftMessage);
					break;
				default:
					REFITLogger.logWarning(this, "unexpected message type in VIEW_CHANGE proof (" + pbftMessage.pbftType +")");
				}
			}

			// Only accept VIEW_CHANGE if the certificate proves progress
			if(!certificate.isPrepared() && !certificate.isCommitted()) return false;
			decision = certificate.message;
		}
		
		// Accept VIEW_CHANGE
		proof.add(viewChange);
		return true;
	}
	
	
	// ######################
	// # CERTIFICATE STATES #
	// ######################

	public boolean isStable() {
		return (proof.size() >= decisionThreshold);
	}
	
}
