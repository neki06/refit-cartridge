package refit.agreement.pbft;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.LinkedList;

import refit.config.REFITConfig;
import refit.message.REFITMessage;
import refit.message.REFITMessageType;
import refit.message.REFITRequest;
import refit.message.REFITUniqueID;
import refit.replica.order.REFITOrderProtocolMessage;
import refit.util.REFITPayload;


public class REFITPBFTMessages {

	// #####################
	// # ABSTRACT MESSAGES #
	// #####################

	public static abstract class REFITPBFTMessage extends REFITOrderProtocolMessage {

		public static final int PBFT_HEADER_SIZE = (Integer.SIZE >> 3);
		
		public final REFITMessageType pbftType;
		public final REFITPayload command;


		protected REFITPBFTMessage(REFITMessageType pbftType, REFITUniqueID uid, long instanceID, int viewID, REFITPayload command, short from) {
			super(instanceID, viewID, uid, from);
			this.pbftType = pbftType;
			this.command = command;
		}
		
		protected REFITPBFTMessage(REFITMessageType pbftType, ByteBuffer buffer) {
			super(buffer);
			this.pbftType = pbftType;
			byte[] payload = new byte[buffer.getInt()];
			buffer.get(payload);
			this.command = new REFITPayload(payload);
		}
		
		
		@Override
		protected void serialize(ByteBuffer buffer) {
			super.serialize(buffer);
			buffer.putInt(command.payload.length);
			buffer.put(command.payload);
		}
		
		@Override
		protected int getHeaderSize() {
			return (super.getHeaderSize() + PBFT_HEADER_SIZE);
		}
		
		@Override
		protected int calculatePayloadSize() {
			return (super.calculatePayloadSize() + PBFT_HEADER_SIZE + command.payload.length);
		}

		@Override
		public byte getTypeMagic() {
			return pbftType.getMagic();
		}

		@Override
		public String toString() {
			return String.format("{%s|%d|%d|%d|%s}", pbftType, from, instanceID, viewID, uid);
		}

	}

	
	// ###########################
	// # OPTIMIZATION FOR NO-OPS #
	// ###########################

	private static REFITPayload createPayloadFromMessage(REFITMessage message) {
		if(message == REFITRequest.NO_OP) return new REFITPayload(new byte[0]);
		return new REFITPayload(message);
	}
	
	private static REFITMessage createMessageFromPayload(REFITPayload command) {
		if(command.payload.length == 0) return REFITRequest.NO_OP;
		return REFITMessage.createMessage(ByteBuffer.wrap(command.payload));
	}
	
	
	// #####################
	// # SPECIFIC MESSAGES #
	// #####################

	public static class REFITPBFTPrePrepare extends REFITPBFTMessage {

		public final transient REFITMessage message;
		
		
		public REFITPBFTPrePrepare(long instanceID, int viewID, REFITMessage message, short from) {
			super(REFITMessageType.PBFT_PRE_PREPARE, message.uid, instanceID, viewID, createPayloadFromMessage(message), from);
			this.message = message;
		}

		public REFITPBFTPrePrepare(ByteBuffer buffer) {
			super(REFITMessageType.PBFT_PRE_PREPARE, buffer);
			this.message = createMessageFromPayload(command);
		}

		
		@Override
		public String toString() {
			return String.format("{%s|%d|%d|%d|%s|%s}", pbftType, from, instanceID, viewID, uid, message);
		}

		@Override
		protected byte[] calculateHash() {
			// Prepare message header
			ByteBuffer messageHeader = getBuffer();
			messageHeader.limit(getHeaderSize());
			
			// Prepare message (must be done in advance in order to prevent the hash from
			// being computed during the calculation of the REFITPBFTPrePrepare hash)
			byte[] messageHash = message.getHash();

			// Calculate hash
			MessageDigest digest = REFITPayload.getDigest();
			digest.update(messageHeader);
			digest.update(messageHash);
			return digest.digest();
		}

	}
	
	public static class REFITPBFTPrepare extends REFITPBFTMessage {

		public REFITPBFTPrepare(REFITPBFTPrePrepare prePrepare, short from) {
			super(REFITMessageType.PBFT_PREPARE, prePrepare.uid, prePrepare.instanceID, prePrepare.viewID, REFITConfig.USE_HASH_BASED_ORDERING ? new REFITPayload(prePrepare.message.getHash()) : prePrepare.command, from);
		}

		public REFITPBFTPrepare(ByteBuffer buffer) {
			super(REFITMessageType.PBFT_PREPARE, buffer);
		}

		
		@Override
		protected byte[] calculateHash() {
			// Prepare message buffer
			ByteBuffer messageBuffer = getBuffer();
			messageBuffer.limit(calculatePayloadSize());

			// Calculate hash
			MessageDigest digest = REFITPayload.getDigest();
			digest.update(messageBuffer);
			return digest.digest();
		}

	}
	
	public static class REFITPBFTCommit extends REFITPBFTMessage {

		public REFITPBFTCommit(REFITPBFTPrePrepare prePrepare, short from) {
			super(REFITMessageType.PBFT_COMMIT, prePrepare.uid, prePrepare.instanceID, prePrepare.viewID, REFITConfig.USE_HASH_BASED_ORDERING ? new REFITPayload(prePrepare.message.getHash()) : prePrepare.command, from);
		}

		public REFITPBFTCommit(ByteBuffer buffer) {
			super(REFITMessageType.PBFT_COMMIT, buffer);
		}

		
		@Override
		protected byte[] calculateHash() {
			// Prepare message buffer
			ByteBuffer messageBuffer = getBuffer();
			messageBuffer.limit(calculatePayloadSize());

			// Calculate hash
			MessageDigest digest = REFITPayload.getDigest();
			digest.update(messageBuffer);
			return digest.digest();
		}

	}

	public static class REFITPBFTViewChange extends REFITPBFTMessage {

		public final transient REFITMessage message;
		public final transient REFITPBFTMessage[] proof;
		
		
		public REFITPBFTViewChange(long instanceID, int viewID, REFITMessage message, REFITPBFTMessage[] proof, short from) {
			super(REFITMessageType.PBFT_VIEW_CHANGE, new REFITUniqueID((short) proof.length, instanceID), instanceID, viewID, createViewChangePayload(message, proof), from);
			this.message = message;
			this.proof = proof;
		}

		public REFITPBFTViewChange(ByteBuffer buffer) {
			super(REFITMessageType.PBFT_VIEW_CHANGE, buffer);
			if(command.payload.length == 0) {
				this.message = REFITRequest.NO_OP;
				this.proof = new REFITPBFTMessage[0];
			} else {
				ByteBuffer payloadBuffer = ByteBuffer.wrap(command.payload);
				this.message = REFITMessage.createMessage(payloadBuffer.slice());
				payloadBuffer.position(payloadBuffer.position() + message.getMessageSize());
				this.proof = new REFITPBFTMessage[uid.nodeID];
				for(int i = 0; i < proof.length; i++) {
					proof[i] = (REFITPBFTMessage) REFITMessage.createMessage(payloadBuffer.slice());
					payloadBuffer.position(payloadBuffer.position() + proof[i].getMessageSize());
				}
			}
		}

		
		private static REFITPayload createViewChangePayload(REFITMessage message, REFITMessage[] proof) {
			// Short cut for no-ops
			if(message == REFITRequest.NO_OP) return new REFITPayload(new byte[0]);
			
			// Determine size
			int size = message.getMessageSize();
			for(REFITMessage msg: proof) size += msg.getMessageSize();
			
			// Pack messages
			ByteBuffer buffer = ByteBuffer.allocate(size);
			buffer.put(message.getBuffer());
			for(REFITMessage msg: proof) buffer.put(msg.getBuffer());
			return new REFITPayload(buffer.array());
		}
		
		@Override
		protected byte[] calculateHash() {
			// Prepare message header
			ByteBuffer messageHeader = getBuffer();
			messageHeader.limit(getHeaderSize());
			
			// Prepare message (must be done in advance in order to prevent the hash from
			// being computed during the calculation of the REFITPBFTPrePrepare hash)
			byte[] messageHash = message.getHash();

			// Prepare proof hashes
			for(REFITPBFTMessage proofMessage: proof) proofMessage.getHash();
			
			// Calculate hash
			MessageDigest digest = REFITPayload.getDigest();
			digest.update(messageHeader);
			digest.update(messageHash);
			for(REFITPBFTMessage proofMessage: proof) digest.update(proofMessage.getHash());
			return digest.digest();
		}
		
	}
	
	public static class REFITPBFTNewView extends REFITPBFTMessage {

		public final transient Collection<REFITPBFTViewChange> proof;
		
		
		public REFITPBFTNewView(long instanceID, int viewID, Collection<REFITPBFTViewChange> proof, short from) {
			super(REFITMessageType.PBFT_NEW_VIEW, new REFITUniqueID((short) proof.size(), instanceID), instanceID, viewID, serializeViewChanges(proof), from);
			this.proof = proof;
		}

		public REFITPBFTNewView(ByteBuffer buffer) {
			super(REFITMessageType.PBFT_NEW_VIEW, buffer);
			this.proof = new LinkedList<REFITPBFTViewChange>();
			short nrOfViewChanges = uid.nodeID;
			ByteBuffer proofBuffer = ByteBuffer.wrap(command.payload);
			for(int i = 0; i < nrOfViewChanges; i++) {
				REFITPBFTViewChange viewChange = new REFITPBFTViewChange(proofBuffer.slice());
				proof.add(viewChange);
				proofBuffer.position(proofBuffer.position() + viewChange.getMessageSize());
			}
		}

		
		private static REFITPayload serializeViewChanges(Collection<REFITPBFTViewChange> viewChanges) {
			// Determine size
			int size = 0;
			for(REFITPBFTViewChange viewChange: viewChanges) size += viewChange.getMessageSize();
			
			// Pack VIEW_CHANGES
			ByteBuffer buffer = ByteBuffer.allocate(size);
			for(REFITPBFTViewChange viewChange: viewChanges) buffer.put(viewChange.getBuffer());
			return new REFITPayload(buffer.array());
		}

		@Override
		protected byte[] calculateHash() {
			// Prepare message header
			ByteBuffer messageHeader = getBuffer();
			messageHeader.limit(getHeaderSize());
			
			// Prepare VIEW_CHANGE hashes
			for(REFITPBFTMessage viewChange: proof) viewChange.getHash();
			
			// Calculate hash
			MessageDigest digest = REFITPayload.getDigest();
			digest.update(messageHeader);
			for(REFITPBFTViewChange viewChange: proof) digest.update(viewChange.getHash());
			return digest.digest();
		}
		
	}
	
}
