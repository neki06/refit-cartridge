package refit.agreement.pbft;

import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTCommit;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrePrepare;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrepare;
import refit.agreement.pbft.REFITPBFTTransition.REFITPBFTTransitionProtocolInstance;
import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITMessage;
import refit.message.REFITMessageType;
import refit.message.REFITRequest;
import refit.replica.REFITMulticastInstruction;
import refit.replica.REFITWorkerStage;
import refit.replica.order.REFITOrderProtocol;
import refit.replica.order.REFITOrderProtocolInstance;
import refit.replica.order.REFITOrderStageSlot;


public class REFITPBFT {

	// ############
	// # PROTOCOL #
	// ############

	public static class REFITPBFTProtocol extends REFITOrderProtocol {

		public static final REFITPBFTProtocol instance = new REFITPBFTProtocol();
		
		
		@Override
		public short getContactReplicaID(short localID) {
			return getPrimary(0L, viewID);
		}
		
		@Override
		public REFITOrderProtocolInstance createInstance(REFITOrderStageSlot slot) {
			return new REFITPBFTProtocolInstance(slot);
		}
		
	}

	
	// #############################
	// # PROTOCOL INSTANCE PRIMARY #
	// #############################

	public static short getPrimary(long instanceID, int viewID) {
		return (short) (viewID % REFITConfig.TOTAL_NR_OF_REPLICAS);
	}

	
	// ############################
	// # PROTOCOL INSTANCE STATES #
	// ############################

	public static enum REFITPBFTProtocolInstanceState {
		INITIALIZED,
		PRE_PREPARED,
		PREPARED,
		COMMITTED
	}

	
	// #####################
	// # PROTOCOL INSTANCE #
	// #####################

	public static class REFITPBFTProtocolInstance extends REFITOrderProtocolInstance {

		protected final REFITPBFTCertificate certificate;
		private REFITPBFTProtocolInstanceState instanceState;
		private boolean isPrimary;
		private REFITMessage proposal;
		
		
		public REFITPBFTProtocolInstance(REFITOrderStageSlot slot) {
			super(slot);
			this.certificate = new REFITPBFTCertificate(slot);
		}

		public REFITPBFTProtocolInstance(REFITOrderStageSlot slot, long instanceID, int viewID, REFITMessage proposal) {
			super(slot);
			this.certificate = new REFITPBFTCertificate(slot);
			init(instanceID, viewID);
			this.proposal = proposal; // Must be after init()
		}

		
		// ##################
		// # HELPER METHODS #
		// ##################
		
		@Override
		public String toString() {
			return "PBFT[" + instanceID + "]";
		}
		
		private void advanceState(REFITPBFTProtocolInstanceState state) {
			if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, instanceID + " reached " + state + " (" + proposal + ", " + certificate.message + ") at slot " + slot);
			this.instanceState = state;
		}

		@Override
		public boolean isProposer() {
			return (isPrimary && (instanceState == REFITPBFTProtocolInstanceState.INITIALIZED));
		}

		protected void orderReplicaMulticast(REFITMessage message) {
			int              salt   = slot.generateMessageSalt();
			REFITWorkerStage worker = slot.getWorker( salt );

			if( worker != null )
				worker.insertMessage( new REFITMulticastInstruction( replica, message, salt, slot.getOrderStage().getAuth().getKey() ) );
			else
			{
				message.serializeMessage(REFITConfig.HMAC_REPLICACAST_SIZE);
				slot.getOrderStage().getAuth().appendMulticastMAC(message);
				replica.replicaMulticast(message, salt);
			}
		}
		
		protected REFITPBFTTransitionProtocolInstance createTransitionProtocolInstance(REFITMessage command) {
			return new REFITPBFTTransitionProtocolInstance(slot, instanceID, viewID + 1, command, certificate.getProof());
		}
		

		// ######################
		// # LIFE-CYCLE METHODS #
		// ######################
		
		@Override
		public void init(long instanceID, int viewID) {
			super.init(instanceID, viewID);
			certificate.init(instanceID, viewID);
			isPrimary = (replica.id == getPrimary(instanceID, viewID));
			proposal = null;
			advanceState(REFITPBFTProtocolInstanceState.INITIALIZED);
		}
		
		@Override
		public REFITOrderProtocolInstance abort() {
			REFITMessage command;
			if(proposal != null) {
				command = proposal;
			} else {
				boolean potentiallyDecided = certificate.isPrepared() || certificate.isCommitted();
				command = potentiallyDecided ? certificate.message : REFITRequest.NO_OP;
			}
			return createTransitionProtocolInstance(command);
		}
		
		
		// ######################
		// # PROTOCOL EXECUTION #
		// ######################

		@Override
		public void execute() {
			if(isPrimary) executePrimary();
			else executeBackup();
		}
		
		private void executePrimary() {
			switch(instanceState) {
			case INITIALIZED:
				// Send PRE_PREPARE
				if(proposal == null) {
					proposal = fetchProposal();
					if(proposal == null) break;
				}
				REFITPBFTPrePrepare prePrepare = new REFITPBFTPrePrepare(instanceID, viewID, proposal, replica.id);
				orderReplicaMulticast(prePrepare);

				// Add PRE_PREPARE to certificate
				certificate.addPrePrepare(prePrepare);

				// Advance to next state
				advanceState(REFITPBFTProtocolInstanceState.PRE_PREPARED);
				//$FALL-THROUGH$
			case PRE_PREPARED:
				executePreparePhase();
				//$FALL-THROUGH$ --> Try to complete commit phase even if prepare phase has not been completed locally
			case PREPARED:
				executeCommitPhase();
				break;
			default:
				REFITLogger.logError(this, "nothing to do for state " + instanceState);
			}
		}
		
		private void executeBackup() {
			switch(instanceState) {
			case INITIALIZED:
				while(true) {
					// Fetch PRE_PREPARE
					REFITPBFTPrePrepare prePrepare = (REFITPBFTPrePrepare) fetchMessage(REFITMessageType.PBFT_PRE_PREPARE, viewID);
					if(prePrepare == null) return;

					// Check whether PRE_PREPARE is from primary
					if(prePrepare.from != getPrimary(instanceID, viewID)) {
						REFITLogger.logWarning(this, "bad PRE_PREPARE " + prePrepare);
						continue;
					}

					// Check whether the proposed message matches the proposal expected (if there is such a proposal)
					// TODO: It should be save to accept non-no-op proposal if the expected proposal is a no-op
					if((proposal != null) && !proposal.equals(prePrepare.message)) {
						REFITLogger.logWarning(this, "bad PRE_PREPARE " + prePrepare + " (message mismatch: " + proposal + " expected)");
						continue;
					}
					
					// Add PRE_PREPARE to certificate
					boolean success = certificate.addPrePrepare(prePrepare);
					if(!success) {
						REFITLogger.logWarning(this, "bad PRE_PREPARE " + prePrepare);
						continue;
					}
					break;
				}
				
				// Create and distribute PREPARE
				REFITPBFTPrepare ownPrepare = new REFITPBFTPrepare(certificate.prePrepare, replica.id);
				orderReplicaMulticast(ownPrepare);

				// Add PREPARE to certificate
				certificate.addPrepare(ownPrepare);
				
				// Advance to next state
				advanceState(REFITPBFTProtocolInstanceState.PRE_PREPARED);
				//$FALL-THROUGH$ --> Try to also complete prepare phase
			case PRE_PREPARED:
				executePreparePhase();
				//$FALL-THROUGH$ --> Try to complete commit phase (even if prepare phase has not been completed locally)
			case PREPARED:
				executeCommitPhase();
				break;
			default:
				REFITLogger.logError(this, "nothing to do for state " + instanceState);
			}
		}

		private void executePreparePhase() {
			while(!certificate.isPrepared()) {
				// Fetch PREPAREs
				REFITPBFTPrepare prepare = (REFITPBFTPrepare) fetchMessage(REFITMessageType.PBFT_PREPARE, viewID);
				if(prepare == null) return;
				
				// Add PREPARE to certificate
				boolean success = certificate.addPrepare(prepare);
				if(!success) REFITLogger.logWarning(this, "bad PREPARE " + prepare);
			}
			
			// Create and distribute COMMIT
			REFITPBFTCommit ownCommit = new REFITPBFTCommit(certificate.prePrepare, replica.id);
			orderReplicaMulticast(ownCommit);

			// Add COMMIT to certificate
			certificate.addCommit(ownCommit);
			
			// Advance to next state
			advanceState(REFITPBFTProtocolInstanceState.PREPARED);
		}
		
		private void executeCommitPhase() {
			while(!certificate.isCommitted()) {
				// Fetch COMMITs
				REFITPBFTCommit commit = (REFITPBFTCommit) fetchMessage(REFITMessageType.PBFT_COMMIT, viewID);
				if(commit == null) return;

				// Add COMMIT to certificate
				boolean success = certificate.addCommit(commit);
				if(!success) REFITLogger.logWarning(this, "bad COMMIT " + commit);
			}

			// Advance to next state
			advanceState(REFITPBFTProtocolInstanceState.COMMITTED);
			
			// Instance complete
			complete(certificate.message, this, true);
		}
		
	}
	
}
