package refit.agreement.pbft;

import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTCommit;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTMessage;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrePrepare;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTPrepare;
import refit.config.REFITConfig;
import refit.message.REFITBatch;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITMessageType;
import refit.replica.order.REFITOrderStageSlot;
import refit.util.REFITBallotBox;
import refit.util.REFITPayload;


public class REFITPBFTCertificate {

	private static final int PREPARED_THRESHOLD  = (2 * REFITConfig.FAULTS_TO_TOLERATE) + 1;
	private static final int COMMITTED_THRESHOLD = (2 * REFITConfig.FAULTS_TO_TOLERATE) + 1;

	private long instanceID;
	private int viewID;
	
	public REFITPBFTPrePrepare prePrepare;
	public REFITMessage message;
	private final REFITBallotBox<Short, REFITPayload, REFITPBFTMessage> prepares;
	private final REFITBallotBox<Short, REFITPayload, REFITPBFTCommit> commits;
	private final REFITMessageAuthentication auth;
	
	
	public REFITPBFTCertificate(REFITOrderStageSlot slot) {
		this.prepares = new REFITBallotBox<Short, REFITPayload, REFITPBFTMessage>(PREPARED_THRESHOLD);
		this.commits = new REFITBallotBox<Short, REFITPayload, REFITPBFTCommit>(COMMITTED_THRESHOLD);
		this.auth = slot.getOrderStage().getAuth();
	}

	
	@Override
	public String toString() {
		return "{" + viewID + " " + instanceID + ": " + isPrepared() + " " + isCommitted() + "}";
	}
	
	public void init(long instanceID, int viewID) {
		this.instanceID = instanceID;
		this.viewID = viewID;
		this.prePrepare = null;
		this.message = null;
		this.prepared = false;
		this.committed = false;
		prepares.clear();
		commits.clear();
	}
	
	
	// ###################
	// # ADDING MESSAGES #
	// ###################

	public boolean addPrePrepare(REFITPBFTPrePrepare prePrepare) {
		if(prePrepare == null) return false;
		if(this.prePrepare != null) return false;
		if(message != null) return false;
		if(!auth.verifyMulticastMAC(prePrepare)) return false;
		if(REFITConfig.ENABLE_BATCHING && (prePrepare.message.type == REFITMessageType.BATCH)) {
			for(REFITMessage msg: ((REFITBatch) prePrepare.message).requests) if(!auth.verifyMulticastMAC(msg)) return false;
		} else if(!auth.verifyMulticastMAC(prePrepare.message)) return false;
		this.prePrepare = prePrepare;
		message = prePrepare.message;
		prepares.add(prePrepare.from, REFITConfig.USE_HASH_BASED_ORDERING ? new REFITPayload(message.getHash()) : prePrepare.command, prePrepare);
		checkPrepared();
		return true;
	}

	public boolean addPrepare(REFITPBFTPrepare prepare) {
		if(prepare == null) return false;
		if(prepares.hasVoted(prepare.from)) return false;
		if(!auth.verifyMulticastMAC(prepare)) return false;
		prepares.add(prepare.from, prepare.command, prepare);
		checkPrepared();
		return true;
	}
	
	public boolean addCommit(REFITPBFTCommit commit) {
		if(commit == null) return false;
		if(commits.hasVoted(commit.from)) return false;
		if(!auth.verifyMulticastMAC(commit)) return false;
		commits.add(commit.from, commit.command, commit);
		checkCommitted();
		return true;
	}

	
	// ######################
	// # CERTIFICATE STATES #
	// ######################
	
	private boolean prepared;
	private boolean committed;
	
	
	public boolean isPrepared() {
		return prepared;
	}

	public boolean isCommitted() {
		return committed;
	}
	
	private void checkPrepared() {
		if(prepared) return;
		if(prePrepare == null) return;
		REFITPayload decision = prepares.getDecision();
		if(decision == null) return;
		if(!prepares.getDecidingBallots().contains(prePrepare)) return;
		prepared = true;
	}
	
	private void checkCommitted() {
		if(committed) return;
		if(!prepared) return;
		if(prePrepare == null) return;
		
		REFITPayload decision = commits.getDecision();
		if(decision == null) return;
		// In the current implementation, a non-faulty replica that
		// has been provided with a faulty PRE_PREPARE will never
		// reach the status "committed" in the corresponding instance,
		// even if it is able to obtain 2f+1 correct COMMITs; see the
		// following code line. This is not a safety problem but
		// implementing a solution (which would require obtaining
		// the correct message) would lead to a more robust protocol.
		if(!decision.matches(REFITConfig.USE_HASH_BASED_ORDERING ? prePrepare.message.getHash() : prePrepare.command.payload)) return;
		committed = true;
	}

	
	// ##################
	// # PROGRESS PROOF #
	// ##################

	public REFITPBFTMessage[] getProof() {
		if(isPrepared()) return createProof(prepares, PREPARED_THRESHOLD);
		return new REFITPBFTMessage[0];
	}

	private static REFITPBFTMessage[] createProof(REFITBallotBox<?, ?, ? extends REFITPBFTMessage> ballotBox, int proofSize) {
		REFITPBFTMessage[] proof = new REFITPBFTMessage[proofSize];
		int i = 0;
		for(REFITPBFTMessage message: ballotBox.getDecidingBallots()) {
			proof[i] = message;
			if(++i >= proofSize) break;
		}
		return proof;
	}
	
}
