package refit.agreement.pbft;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import refit.agreement.pbft.REFITPBFT.REFITPBFTProtocol;
import refit.agreement.pbft.REFITPBFT.REFITPBFTProtocolInstance;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTMessage;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTNewView;
import refit.agreement.pbft.REFITPBFTMessages.REFITPBFTViewChange;
import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITMessage;
import refit.message.REFITMessageAuthentication;
import refit.message.REFITMessageType;
import refit.replica.order.REFITOrderProtocolInstance;
import refit.replica.order.REFITOrderStageSlot;



public class REFITPBFTTransition {

	// ############################
	// # PROTOCOL INSTANCE STATES #
	// ############################

	public static enum REFITPBFTTransitionProtocolState {
		INITIALIZED,
		VIEW_CHANGE_SENT,
		COMPLETE
	}

	
	// #####################
	// # PROTOCOL INSTANCE #
	// #####################

	public static class REFITPBFTTransitionProtocolInstance extends REFITOrderProtocolInstance {

		private static final int CREATION_THRESHOLD = (2 * REFITConfig.FAULTS_TO_TOLERATE) + 1;
		private static final int VERIFICATION_THRESHOLD = CREATION_THRESHOLD - REFITConfig.FAULTS_TO_TOLERATE;

		private final Map<Integer, REFITPBFTTransitionCertificate> certificates;
		private REFITPBFTViewChange ownViewChange;
		private REFITPBFTTransitionProtocolState instanceState;
		
		
		public REFITPBFTTransitionProtocolInstance(REFITOrderStageSlot slot, long instanceID, int viewID, REFITMessage message, REFITPBFTMessage[] proof) {
			super(slot);
			this.certificates = new HashMap<Integer, REFITPBFTTransitionCertificate>();
			this.instanceState = REFITPBFTTransitionProtocolState.INITIALIZED;
			init(instanceID, viewID); // Set instance ID and view ID
			this.ownViewChange = createOwnViewChange(message, proof);
		}

		
		// ##################
		// # HELPER METHODS #
		// ##################
		
		@Override
		public String toString() {
			return "VWCH[" + instanceID + "-" + viewID + "]";
		}

		private void advanceState(REFITPBFTTransitionProtocolState state) {
			this.instanceState = state;
		}

		@Override
		public boolean isProposer() {
			return false;
		}

		
		// ###############################
		// # TRANSITION-SPECIFIC METHODS #
		// ###############################

		protected REFITPBFTViewChange createOwnViewChange(REFITMessage message, REFITPBFTMessage[] proof) {
			return new REFITPBFTViewChange(instanceID, viewID, message, proof, replica.id);
		}
		
		protected REFITPBFTTransitionCertificate createCertificate(boolean localPrimary) {
			return new REFITPBFTTransitionCertificate(localPrimary ? CREATION_THRESHOLD : VERIFICATION_THRESHOLD, slot);
		}
		
		protected boolean checkViewChange(REFITPBFTViewChange viewChange) {
			return (viewID <= viewChange.viewID);
		}
		
		
		// ######################
		// # LIFE-CYCLE METHODS #
		// ######################

		@Override
		public REFITOrderProtocolInstance abort() {
			// Garbage-collect old certificate
			certificates.remove(viewID);
			
			// Reset state and move to next view 
			instanceState = REFITPBFTTransitionProtocolState.INITIALIZED;
			viewID++;
			
			// Create new own VIEW_CHANGE
			if(ownViewChange != null) ownViewChange = createOwnViewChange(ownViewChange.message, ownViewChange.proof);
			return this;
		}
		
		@Override
		public void execute() {
			switch(instanceState) {
			case INITIALIZED:
				if(ownViewChange != null) {
					// Serialize own VIEW_CHANGE
					ownViewChange.serializeMessage(REFITConfig.HMAC_REPLICACAST_SIZE);
					slot.getOrderStage().getAuth().appendSignature(ownViewChange);
					
					// Store own VIEW_CHANGE or send it to the view-change primary
					short viewChangePrimary = REFITPBFT.getPrimary(instanceID, viewID);
					if(replica.id == viewChangePrimary) {
						// Store own VIEW_CHANGE if this replica is the primary for the particular view change
						boolean instanceComplete = storeViewChangePrimary(ownViewChange);
						if(instanceComplete) return;
					} else {
						// Send own VIEW_CHANGE to the primary for the particular view change
						if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "send " + ownViewChange + " to " + viewChangePrimary);
						replica.replicaUnicast(ownViewChange, viewChangePrimary, slot.generateMessageSalt());
					}
				}
				
				// Advance to next state
				advanceState(REFITPBFTTransitionProtocolState.VIEW_CHANGE_SENT);
				//$FALL-THROUGH$
			case VIEW_CHANGE_SENT:
				// Process NEW_VIEWs
				while(true) {
					// Get NEW_VIEW
					REFITPBFTNewView newView = (REFITPBFTNewView) fetchMessage(REFITMessageType.PBFT_NEW_VIEW);
					if(newView == null) break;
					if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "received NEW_VIEW " + newView);

					// Check NEW_VIEW
					if(newView.from != REFITPBFT.getPrimary(instanceID, newView.viewID)) continue;
					if(!slot.getOrderStage().getAuth().verifySignature(newView)) continue;

					// Verify and get decision
					for(REFITPBFTViewChange viewChange: newView.proof) {
						Collection<REFITPBFTViewChange> stabilityProof = storeViewChange(viewChange);
						if(stabilityProof != null) return;
					}
					REFITLogger.logWarning(this, "bad NEW_VIEW " + newView);
				}
				
				// Process VIEW_CHANGEs
				while(true) {
					// Get VIEW_CHANGE
					REFITPBFTViewChange viewChange = (REFITPBFTViewChange) fetchMessage(REFITMessageType.PBFT_VIEW_CHANGE);
					if(viewChange == null) break;
					if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "received VIEW_CHANGE " + viewChange + " (" + (replica.id == REFITPBFT.getPrimary(instanceID, viewChange.viewID)) + ")");
					
					// Check VIEW_CHANGE
					if(replica.id != REFITPBFT.getPrimary(instanceID, viewChange.viewID)) continue;
					if(!checkViewChange(viewChange)) continue;
					
					// Store VIEW_CHANGE
					boolean instanceComplete = storeViewChangePrimary(viewChange);
					if(instanceComplete) return;
				}
				break;
			default:
				REFITLogger.logError(this, "nothing to do for state " + instanceState);
			}
		}
		
		private Collection<REFITPBFTViewChange> storeViewChange(REFITPBFTViewChange viewChange) {
			// Get certificate
			REFITPBFTTransitionCertificate certificate = certificates.get(viewID);
			if(certificate == null) {
				certificate = createCertificate(replica.id == REFITPBFT.getPrimary(instanceID, viewChange.viewID));
				certificates.put(viewID, certificate);
			}

			// Add VIEW_CHANGE to certificate
			boolean success = certificate.addViewChange(viewChange, replica.id == viewChange.from);
			if(!success) {
				REFITLogger.logWarning(this, "bad VIEW_CHANGE " + viewChange);
				return null;
			}
			
			// Check whether the certificate has become stable
			if(!certificate.isStable()) return null;
			if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "certificate for view " + viewChange.viewID + " became stable");
			
			// Announce new view
			updateView(viewChange.viewID, REFITPBFTProtocol.instance);
			
			// Create new PBFT instance
			REFITPBFTProtocolInstance pbftInstance = new REFITPBFTProtocolInstance(slot, instanceID, viewChange.viewID, certificate.decision);
			if(REFITLogger.LOG_ORDER) REFITLogger.logOrder(this, "switch to PBFT instance for " + certificate.decision);
			
			// Switch to PBFT
			advanceState(REFITPBFTTransitionProtocolState.COMPLETE);
			complete(null, pbftInstance, false);
			return certificate.proof;
		}
		
		private boolean storeViewChangePrimary(REFITPBFTViewChange viewChange) {
			// Store VIEW_CHANGE
			Collection<REFITPBFTViewChange> stabilityProof = storeViewChange(viewChange);
			if(stabilityProof == null) return false;

			// Create and distribute NEW_VIEW
			REFITPBFTNewView newView = new REFITPBFTNewView(instanceID, viewChange.viewID, stabilityProof, replica.id);
			newView.serializeMessage(REFITConfig.HMAC_REPLICACAST_SIZE);
			slot.getOrderStage().getAuth().appendSignature(newView);
			replica.replicaMulticast(newView, slot.generateMessageSalt());
			return true;
		}
		
	}
	
}
