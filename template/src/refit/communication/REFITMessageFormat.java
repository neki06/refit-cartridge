package refit.communication;

import java.nio.ByteBuffer;

import refit.message.REFITMessage;


public class REFITMessageFormat
{
	public ByteBuffer getSendBuffer(ByteBuffer msgbuf)
	{
		return msgbuf;
	}


	public boolean tryWriteMessage(ByteBuffer out, ByteBuffer msgbuf)
	{
		if( out.remaining()<msgbuf.remaining() )
			return false;
		else
		{
			out.put( msgbuf );
			return true;
		}
	}


	public ByteBuffer tryReadMessage(ByteBuffer in)
	{
		if( in.remaining()<(Integer.SIZE>>3) )
			return null;

		int msgsize = in.getInt( in.position() );

		if( in.remaining()<msgsize)
			return null;

		ByteBuffer msgbuf = ByteBuffer.allocate( msgsize );
		in.get( msgbuf.array() );

		return msgbuf;
	}


	public ByteBuffer serializeMessage(REFITMessage msg)
	{
		return msg.getBuffer();
	}


	public REFITMessage deserializeMessage(ByteBuffer msgbuf)
	{
		return REFITMessage.createMessage( msgbuf );
	}
}
