package refit.communication;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;

import refit.config.REFITConfig;
import refit.config.REFITLogger;
import refit.message.REFITMessage;
import refit.replica.REFITReplica;


public class REFITReplicaNetworkEndpoint {

	private final REFITReplica replica;
	private final REFITReplicaNetworkEndpointWorker[][] workers;

	
	public REFITReplicaNetworkEndpoint(REFITReplica replica) {
		this.replica = replica;
		this.workers = new REFITReplicaNetworkEndpointWorker[REFITConfig.TOTAL_NUMBER_OF_REPLICA_NETWORKS][REFITConfig.TOTAL_NR_OF_REPLICAS];
	}

	
	@Override
	public String toString() {
		return "RPNET";
	}
	

	// ############################
	// # ESTABLISHING CONNECTIONS #
	// ############################

	public void init() {
		// Establish connections to all other replicas
		REFITReplicaNetworkEndpointAcceptor[] acceptors = new REFITReplicaNetworkEndpointAcceptor[ REFITConfig.NUMBER_OF_REPLICA_ADDRESSES ];
		for( int i = 0; i < REFITConfig.NUMBER_OF_REPLICA_ADDRESSES; i++ )
		{
			acceptors[i] = new REFITReplicaNetworkEndpointAcceptor(i);
			acceptors[i].start();
		}

		for( short i = 0, net = 0; i < REFITConfig.NUMBER_OF_REPLICA_ADDRESSES; i++ )
		{
			for( short j = 0; j < REFITConfig.NUMBER_OF_REPLICA_NETWORKS[i]; j++, net++ )
			{
				for(short k = (short) (replica.id + 1); k < REFITConfig.TOTAL_NR_OF_REPLICAS; k++) {
					SocketAddress locaddr = REFITConfig.ADDRESSES[replica.id][i];
					SocketAddress remaddr = REFITConfig.ADDRESSES[k][i];
					REFITConnector connector = REFITConnector.connect( net, locaddr, remaddr );

					if( REFITLogger.LOG_EVENT )
						REFITLogger.logEvent( this, String.format( "Established connection %s -> %s at net %d", locaddr, remaddr, net ) );

					workers[net][k] = new REFITReplicaNetworkEndpointWorker(replica, net, k, connector.socketChannel);
				}
			}
		}

		try
		{
			for( REFITReplicaNetworkEndpointAcceptor acceptor : acceptors )
				acceptor.join();
		} catch (InterruptedException e) { }
	}


	private class REFITReplicaNetworkEndpointAcceptor extends Thread {

		private int addrno;

		public REFITReplicaNetworkEndpointAcceptor(int addrno)
		{
			this.addrno = addrno;
		}
		@Override
		public void run() {
			try {
				// Create server socket channel
				ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
				serverSocketChannel.socket().bind(REFITConfig.ADDRESSES[replica.id][addrno]);

				// Accept replica connections
				for(int i = 0; i < replica.id * REFITConfig.NUMBER_OF_REPLICA_NETWORKS[addrno]; i++) {
					REFITConnector connector = REFITConnector.accept(serverSocketChannel);
					workers[connector.remoteNet][connector.remoteID] = new REFITReplicaNetworkEndpointWorker(replica, connector.remoteNet, connector.remoteID, connector.socketChannel);
					REFITConnector.finishAccept(connector);
				}

				// Close server socket channel
				serverSocketChannel.close();
			} catch(IOException ioe) {
				ioe.printStackTrace();
				System.exit(1);
			}
		}

	}
	
	
	// #######################
	// # UNICAST & MULTICAST #
	// #######################

	public void replicaUnicast(int net, REFITMessage message, short replicaID) {
		if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication(this, "unicast message " + message + " to " + replicaID + " (" + REFITConfig.ADDRESSES[replicaID] + ")" + " via net " + net);
		workers[net][replicaID].enqueueMessage(message.getBuffer());
	}

	public void replicaMulticast(int net, REFITMessage message) {
		if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication(this, "multicast message " + message + " via net " + net);
		handleMessage(net, message, null);
	}

	public void replicaMulticast(int net, REFITMessage message, boolean[] replicas) {
		if(REFITLogger.LOG_COMMUNICATION) REFITLogger.logCommunication(this, "multicast message " + message + " via net " + net);
		handleMessage(net, message, replicas);
	}

	
	// ########################
	// # SEND BUFFER HANDLING #
	// ########################
	
	private synchronized void handleMessage(int net, REFITMessage message, boolean[] replicas) {
		ByteBuffer buffer = message.getBuffer();
		for(int i = 0; i < workers[net].length; i++) {
			if(i == replica.id) continue;
			if((replicas == null) || replicas[i]) workers[net][i].enqueueMessage(buffer.slice());
		}
	}

}
