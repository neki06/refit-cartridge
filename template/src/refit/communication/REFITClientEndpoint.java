package refit.communication;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import refit.config.REFITConfig;
import refit.message.REFITMessage;
import refit.scheduler.REFITSchedulerTask;


public class REFITClientEndpoint {

	private final short clientID;
	private final REFITConnection[] connections;
	private final REFITMessageFormat msgformat;

	
	public REFITClientEndpoint(short clientID, REFITMessageFormat msgformat) {
		this.clientID = clientID;
		this.connections = new REFITConnection[REFITConfig.TOTAL_NR_OF_REPLICAS];
		this.msgformat = msgformat;
	}
	
	
	@Override
	public String toString() {
		return "CLTEP[" + clientID + "]";
	}

	public void init() {
		Random rnd = new Random( clientID );
		// Establish connections to all replicas
		for(short i = 0; i < connections.length; i++) {
			int[] addrs   = REFITConfig.CLIENTSTAGE.getAddresses( clientID % REFITConfig.CLIENTSTAGE.getNumber() );
			int   addridx =	addrs[ Math.abs( rnd.nextInt() ) % addrs.length ];
			REFITConnector connector = REFITConnector.connect((short)0, REFITConfig.ADDRESSES[clientID][0], REFITConfig.ADDRESSES[i][addridx]);
			if(connector == null) continue;
			connections[i] = new REFITConnection(REFITConfig.CLIENT_RECEIVE_BUFFER_SIZE, REFITConfig.CLIENT_SEND_BUFFER_SIZE, msgformat);
			connections[i].init(connector.socketChannel);
		}
	}
	
	public void register(REFITSchedulerTask task) {
		for(REFITConnection connection: connections) {
			connection.register(task);
		}
	}


	public void executeSends() {
		for(REFITConnection connection: connections) {
			connection.send();
		}
	}
	
	public List<REFITMessage> executeReceives(Set<SelectionKey> readyKeys) {
		List<REFITMessage> messages = new LinkedList<REFITMessage>();
		for(REFITConnection connection: connections) {
			// Skip connection if there is nothing to be done
			if(!readyKeys.contains(connection.selectionKey)) continue;

			// Handle receiving
			List<ByteBuffer> receivedMessages = connection.receive();
			for(ByteBuffer receivedMessage: receivedMessages) {
				REFITMessage message = msgformat.deserializeMessage( receivedMessage );
				messages.add(message);
			}
			receivedMessages.clear();
		}
		return messages;
	}
	
	
	// ####################
	// # SENDING MESSAGES #
	// ####################

	public void sendToReplica(REFITMessage message, short replicaID) {
		connections[replicaID].enqueue( msgformat.serializeMessage( message ) );
	}
	
	public void sendToAllReplicas(REFITMessage message) {
		for(int i = 0; i < connections.length; i++) {
			connections[i].enqueue( msgformat.serializeMessage( message ) );
		}
	}

}
