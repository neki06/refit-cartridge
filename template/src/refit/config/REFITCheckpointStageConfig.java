package refit.config;

import java.util.Properties;

public class REFITCheckpointStageConfig extends REFITStageConfig
{
	private final int[][] map_to_nets;
	private final int[]   map_to_exectstages;
	private final boolean propagate_to_all;

	private REFITCheckpointStageConfig(Properties props)
	{
		super( props, "chkpt" );

		map_to_nets        = loadArrayMapping( props, "chkpt", "network", new int[] { 0 } );
		map_to_exectstages = loadMapping( props, "chkpt", "exect", 0 );
		propagate_to_all   = REFITConfig.getBoolProp( props, getKey( "chkpt", "toall" ), true );
	}

	public static REFITCheckpointStageConfig load(Properties props)
	{
		return new REFITCheckpointStageConfig( props );
	}

	public final int[] getNetworkIDs(int chkptid)
	{
		return map_to_nets[ chkptid ];
	}

	public final int getExecutionStageID(int chkptid)
	{
		return map_to_exectstages[ chkptid ];
	}

	public final int[] getMapToExecutionStages()
	{
		return map_to_exectstages;
	}

	public final boolean propagateToAll()
	{
		return propagate_to_all;
	}
}
