package refit.config;

import java.util.Properties;

public class REFITWorkerStageConfig extends REFITStageConfig
{
	private final boolean use_for_verification;

	private REFITWorkerStageConfig(Properties props)
	{
		super( props, "workr" );

		use_for_verification = REFITConfig.getBoolProp( props, getKey( "workr", "use_for_verification" ), false );
	}

	public static REFITWorkerStageConfig load(Properties props)
	{
		return new REFITWorkerStageConfig( props );
	}

	public boolean useForVerification()
	{
		return use_for_verification;
	}
}
