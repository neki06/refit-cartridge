package refit.config;

import java.util.Properties;

public class REFITOrderStageConfig extends REFITStageConfig
{
	private final int[][] map_to_nets;
	private final int[][] map_to_workers;
	private final int[]   map_to_clientstages;
	private final int[]   map_to_exectstages;

	private REFITOrderStageConfig(Properties props)
	{
		super( props, "order" );

		map_to_nets         = loadArrayMapping( props, "order", "network", new int[] { 0 } );
		map_to_workers      = loadArrayMapping( props, "order", "worker", null );
		map_to_clientstages = loadMapping( props, "order", "client", 0 );
		map_to_exectstages  = loadMapping( props, "order", "exect", 0 );
	}

	public static REFITOrderStageConfig load(Properties props)
	{
		return new REFITOrderStageConfig( props );
	}

	public final int[] getNetworkIDs(int orderid)
	{
		return map_to_nets[ orderid ];
	}

	public final int[] getWorkerIDs(int orderid)
	{
		return map_to_workers[ orderid ];
	}

	public final int getClientStageID(int orderid)
	{
		return map_to_clientstages[ orderid ];
	}

	public final int getExecutionStageID(int orderid)
	{
		return map_to_exectstages[ orderid ];
	}

	public final int[] getMapToClientStages()
	{
		return map_to_clientstages;
	}
}
