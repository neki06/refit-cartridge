package refit.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import refit.agreement.pbft.REFITPBFT.REFITPBFTProtocol;
import refit.replica.order.REFITOrderProtocol;
import refit.scheduler.REFITScheduler;
import refit.scheduler.REFITSchedulerTask;
import refit.scheduler.REFITSchedulerTaskType;


public class REFITConfig {

	// Cell
	public static       short   TOTAL_NR_OF_CLIENTS;
	public static       short   TOTAL_NR_OF_CLIENTHOSTS;
	public static       short[] NR_OF_CLIENTS_PER_HOST;
	public static final short   TOTAL_NR_OF_REPLICAS = 4;
	public static final short   FAULTS_TO_TOLERATE   = 1;

	// Intervals and timeouts
	public static       boolean CLIENT_USE_REQUEST_TIMEOUT;
	public static final int CLIENT_REQUEST_TIMEOUT  = 4000;
	public static final int REPLICA_REQUEST_TIMEOUT = CLIENT_REQUEST_TIMEOUT - 100;
	public static final int REPLICA_PANIC_INTERVAL  = 5000;

	// Client-replica & replica-replica communication
	public static       REFITClientStageConfig CLIENTSTAGE;
	public static final boolean   AUTHENTICATE_MESSAGES = true;
	public static       String    HMAC_ALGORITHM;                // "HmacSha1" || "SHA-256"
	public static       boolean   HMAC_DIGESTONLY;
	public static       int       HMAC_UNICAST_SIZE;
	public static       int       HMAC_REPLICACAST_SIZE;
	public static       int       SIGNATURE_SIZE;
	public static final String    HMAC_KEYALGORTHM      = "PBEWithMD5AndDES";
	public static 		boolean   HMAC_UNIFIEDKEYS;
	public static final boolean   USE_HASHED_REPLIES    = false;
	public static       boolean   VERIFY_REPLIES;
	public static       boolean   CARRYOUT_HANDSHAKE;

	// Checkpoints
	public static       REFITCheckpointStageConfig CHECKPOINTSTAGE;
	public static       int     CHECKPOINT_INTERVAL;
	public static final boolean DISTRIBUTE_CHECKPOINT_HASHES = false;

	// Order stage
	public static       REFITOrderStageConfig ORDERSTAGE;
	public static       int     MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_FACTOR;
	public static       int     MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_PER_STAGE;
	public static       int     MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL;
	public static final boolean USE_HASH_BASED_ORDERING             = true;
	public static       boolean ENABLE_BATCHING;
	public static       int     MINIMUM_BATCH_SIZE;
	public static       int     MAXIMUM_BATCH_SIZE;

	// Execution stage
	public static       REFITExecutionStageConfig EXECUTIONSTAGE;

	// Worker stages
	public static       REFITWorkerStageConfig WORKERSTAGE;
	// PBFT / Passive PBFT
	public static final REFITOrderProtocol INITIAL_ORDER_PROTOCOL                 = REFITPBFTProtocol.instance;
	public static final int                REGULAR_CHECKPOINT_STABILITY_THRESHOLD = (2 * REFITConfig.FAULTS_TO_TOLERATE) + 1;

	// Debugging and statistics
	public static final boolean ENABLE_DEBUG_CHECKS        = false;
	public static final boolean COLLECT_STAGE_STATISTICS   = false;
	public static final boolean COLLECT_MESSAGE_STATISTICS = false;
	public static       long    TIMELOG_RESOL;
	public static       String  RESULTS_PATH;

	// Benchmark workload
	public static       int REQUEST_SIZE;
	public static       int REPLY_SIZE;

	// Communication
	public static       int     NUMBER_OF_REPLICA_ADDRESSES;
	public static       int     TOTAL_NUMBER_OF_REPLICA_NETWORKS;
	public static       int[]   NUMBER_OF_REPLICA_NETWORKS;
	public static final boolean TCP_NO_DELAY                        = true;
	public static       int     REPLICA_NETWORK_SEND_BUFFER_SIZE;
	public static       int     REPLICA_NETWORK_RECEIVE_BUFFER_SIZE;
	public static       int     CLIENT_NETWORK_RECEIVE_BUFFER_SIZE;
	public static       int     CLIENT_RECEIVE_BUFFER_SIZE;
	public static       int     CLIENT_SEND_BUFFER_SIZE;



	public static SocketAddress[][] ADDRESSES;

	// Scheduling
	public static REFITSchedulingConfig CLIENT_SCHEDULING;
	public static REFITSchedulingConfig REPLICA_SCHEDULING;


	public static final void printSchedulingConfig(REFITSchedulingConfig schedconf, REFITScheduler[] scheds)
	{
		if( !REFITLogger.LOG_INFO )
			return;

		String aff = getIntArrayPropString( schedconf.getProcessAffinity() );

		REFITLogger.logInfo( "CONFIG", "Process affinity: " + aff );

		int i = 0;

		for( REFITScheduler s : scheds )
		{
			aff = getIntArrayPropString( s.getAffinity() );
			REFITLogger.logInfo( "CONFIG", String.format( "Scheduler %3d: %s, affinity %s", i++, s.getName(), aff ) );

			for( REFITSchedulerTask t : s.getAssignedTasks() )
				REFITLogger.logInfo( "CONFIG", String.format( "  Task %3d: %-20s (%s)", t.id, t, t.taskType ) );
		}
	}


	public static final void load(String path) throws IOException
	{
		try( FileInputStream cfs = new FileInputStream( path ) )
		{
			Properties props = new Properties();
			props.load( cfs );

			REQUEST_SIZE               = getIntProp( props, "benchmark.request_size", 0 );
			REPLY_SIZE                 = getIntProp( props, "benchmark.reply_size", 0 );
			VERIFY_REPLIES             = getBoolProp( props, "benchmark.verify_replies", true );
			CLIENT_USE_REQUEST_TIMEOUT = getBoolProp( props, "benchmark.use_request_timeout", true );
			TOTAL_NR_OF_CLIENTS        = (short) getIntProp( props, "clients.number", 10 );
			TOTAL_NR_OF_CLIENTHOSTS    = (short) getIntProp( props, "clients.hosts", 1 );
			CARRYOUT_HANDSHAKE         = getBoolProp( props, "clients.handshake", true );

			MINIMUM_BATCH_SIZE  = getIntProp( props, "agreement.batchsize_min", 1 );
			MAXIMUM_BATCH_SIZE  = getIntProp( props, "agreement.batchsize_max", Integer.MAX_VALUE );
			ENABLE_BATCHING     = MAXIMUM_BATCH_SIZE > 1;
			CHECKPOINT_INTERVAL = getIntProp( props, "agreement.checkpoint_interval", 100 );
			MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_FACTOR = getIntProp( props, "agreement.in_progress_factor", 5 );

			WORKERSTAGE = REFITWorkerStageConfig.load( props );

			ORDERSTAGE = REFITOrderStageConfig.load( props );

			int nos = ORDERSTAGE.getNumber();

			MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_PER_STAGE =
					(CHECKPOINT_INTERVAL * MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_FACTOR + (nos-1)) / nos;
			MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_TOTAL     =
					MAXIMUM_ORDER_INSTANCES_IN_PROGRESS_PER_STAGE * nos;

			CHECKPOINTSTAGE = REFITCheckpointStageConfig.load( props );
			EXECUTIONSTAGE  = REFITExecutionStageConfig.load( props, CHECKPOINTSTAGE );

			REPLICA_NETWORK_SEND_BUFFER_SIZE    = getIntProp( props, "networks.replica.send_buffer", 1024 * 1024 * 64 );
			REPLICA_NETWORK_RECEIVE_BUFFER_SIZE = getIntProp( props, "networks.replica.recv_buffer", 1024 * 1024 * 64 );
			CLIENT_NETWORK_RECEIVE_BUFFER_SIZE  = getIntProp( props, "networks.replica.client_recv_buffer", 1024 * 16 );
			CLIENT_SEND_BUFFER_SIZE             = getIntProp( props, "networks.client.send_buffer", 1024 * 8 );
			CLIENT_RECEIVE_BUFFER_SIZE          = getIntProp( props, "networks.client.recv_buffer", 1024 * 16 );

			NUMBER_OF_REPLICA_ADDRESSES = getIntProp( props, "networks.replica.addrs", 1 );

			TOTAL_NUMBER_OF_REPLICA_NETWORKS = 0;
			NUMBER_OF_REPLICA_NETWORKS = new int[ NUMBER_OF_REPLICA_ADDRESSES ];

			for( int i = 0; i < NUMBER_OF_REPLICA_ADDRESSES; i++ )
			{
				NUMBER_OF_REPLICA_NETWORKS[i] = getIntProp( props, "networks.replica.number." + i, 1 );
				TOTAL_NUMBER_OF_REPLICA_NETWORKS += NUMBER_OF_REPLICA_NETWORKS[i];
			}

			CLIENT_SCHEDULING  = REFITSchedulingConfig.load( props, "client", "CLIENT",
					Collections.<REFITSchedulerTaskType>emptyList() );
			REPLICA_SCHEDULING = REFITSchedulingConfig.load( props, "replica", "REPLICA",
					Arrays.asList( REFITSchedulerTaskType.values() ) );

			ADDRESSES = new SocketAddress[TOTAL_NR_OF_REPLICAS + TOTAL_NR_OF_CLIENTS][];

			for(int i = 0; i < TOTAL_NR_OF_REPLICAS; i++) {
				ADDRESSES[i] = new SocketAddress[ NUMBER_OF_REPLICA_ADDRESSES ];

				for( int j = 0; j < NUMBER_OF_REPLICA_ADDRESSES; j++ )
				{
					String[] ep = props.getProperty( "addresses.server." + i + "." + j ).split( ":" );

					ADDRESSES[i][j] = new InetSocketAddress( ep[0], Integer.parseInt( ep[1] ) ); // Replica addresses
				}
			}

			int[] replicaaddrs = new int[ NUMBER_OF_REPLICA_ADDRESSES ];
			for( int i = 0; i < NUMBER_OF_REPLICA_ADDRESSES; i++ )
				replicaaddrs[i] = i;

			CLIENTSTAGE = REFITClientStageConfig.load( props, ORDERSTAGE, replicaaddrs );

			NR_OF_CLIENTS_PER_HOST = new short[ TOTAL_NR_OF_CLIENTHOSTS ];

			for( int c = 0; c < TOTAL_NR_OF_CLIENTHOSTS; c++ )
			{
				String[] clientep   = props.getProperty( "addresses.clients." + c ).split( ":" );
				int      clientport = Integer.parseInt( clientep[1] );

				short j = 0;
				for( int i = c; i < (ADDRESSES.length - TOTAL_NR_OF_REPLICAS); i += TOTAL_NR_OF_CLIENTHOSTS )
					ADDRESSES[i + TOTAL_NR_OF_REPLICAS] = new SocketAddress[] { new InetSocketAddress( clientep[0], clientport + j++) }; // Client addresses
				NR_OF_CLIENTS_PER_HOST[c] = j;
			}

			for(short i = 0; i < ADDRESSES.length; i++) {
				for( SocketAddress addr : ADDRESSES[i] )
					addresses.put(addr, i);
			}

			TIMELOG_RESOL = getLongProp( props, "stats.timelog.resol", 10 );
			RESULTS_PATH  = new File( path ).getParent();

			initCrypto( props );
		}
	}


	private static void initCrypto(Properties props)
    {
	    HMAC_UNIFIEDKEYS = getBoolProp( props, "crypto.unifiedkey", true );
	    HMAC_ALGORITHM   = props.getProperty( "crypto.hmac_algo", "HmacSha1" );

	    switch( HMAC_ALGORITHM )
	    {
	    case "SHA-256":
	    	HMAC_UNICAST_SIZE = 32;
	    	HMAC_DIGESTONLY = true;
	    	break;

	    case "HmacSha1":
	    	HMAC_UNICAST_SIZE = 20;
	    	HMAC_DIGESTONLY = false;
	    	break;

	    default:
	    	throw new IllegalStateException( "Unknown HMAC algorithm " + HMAC_ALGORITHM );
	    }

	    HMAC_REPLICACAST_SIZE = HMAC_UNICAST_SIZE * TOTAL_NR_OF_REPLICAS;
	    SIGNATURE_SIZE        = HMAC_REPLICACAST_SIZE;
    }


	static final boolean getBoolProp(Properties props, String key, boolean def)
	{
		String val = props.getProperty( key );

		return val!=null ? Boolean.parseBoolean( val ) : def;
	}



	static final int getIntProp(Properties props, String key, int def)
	{
		String val = props.getProperty( key );

		return val!=null ? Integer.parseInt( val ) : def;
	}


	static final long getLongProp(Properties props, String key, long def)
	{
		String val = props.getProperty( key );

		return val!=null ? Long.parseLong( val ) : def;
	}


	static final int[] getIntArrayProp(Properties props, String key, int[] def)
	{
		String val = props.getProperty( key );

		if( val==null || val.isEmpty() )
			return def;
		else
		{
			List<Integer> array = new LinkedList<>();

			String[] parts = val.split( "\\s*,\\s*" );

			for( String p : parts )
			{
				if( !p.contains( "-" ) )
					array.add( Integer.parseInt( p ) );
				else
				{
					String[] r = p.split( "\\s*-\\s*", 2 );
					int      c = Integer.parseInt( r[0] );
					int      l = Integer.parseInt( r[1] );

					while( c<=l ) array.add( c++ );
				}
			}

			int[] ret = new int[ array.size() ];
			int   i   = 0;
			for( Integer p : array ) ret[ i++ ] = p;

			return ret;
		}
	}


	static final String getIntArrayPropString(int[] array)
	{
		if( array==null )
			return "-";

		StringBuilder sb = new StringBuilder();
		int           l  = 0;
		boolean       r  = false;

		for( int i = 0; i < array.length; i++ )
		{
			int c = array[i];

			if( i>0 && l==c-1 )
			{
				r = true;

				if( i==array.length-1 )
					sb.append( "-" ).append( c );
			}
			else
			{
				if( r )
					sb.append( "-" ).append( l );

				if( i > 0 )
					sb.append( ", " );

				sb.append( c );
				r = false;
			}

			l = c;
		}

		return sb.toString();
	}


	static int[][] reverseIndex(int[] idx, int targetno)
	{
		int[] cnt = new int[ targetno ];

		for( int e : idx )
			cnt[ e ]++;

		int[][] revidx = new int[ targetno ][];

		for( int target = 0; target < targetno; target++ )
		{
			revidx[ target ] = new int[ cnt[ target ] ];

			for( int source = 0, i = 0; source < idx.length; source++ )
				if( idx[ source ] == target )
					revidx[ target ][ i++ ] = source;
		}

		return revidx;
	}


	private static final Map<SocketAddress, Short> addresses = new HashMap<SocketAddress, Short>();

	public static short getNodeByAddress(SocketAddress address) {
		return addresses.get(address);
	}

}
