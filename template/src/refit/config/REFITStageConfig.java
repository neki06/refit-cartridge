package refit.config;

import java.util.Arrays;
import java.util.Properties;

public abstract class REFITStageConfig
{
	private final int    number;

	protected REFITStageConfig(Properties props, String group)
	{
		number = REFITConfig.getIntProp( props, getKey( group, "number" ), 1 );
	}

	public final int getNumber()
	{
		return number;
	}

	protected String getKey(String group, String add)
	{
		return "stages." + group + "." + add;
	}

	protected int[] loadMapping(Properties props, String group, String name, int def)
	{
		int[] range = REFITConfig.getIntArrayProp( props, getKey( group, name ), null );

		if( range == null )
		{
			int[] map = new int[ number ];

			for( int i = 0; i < number; i++ )
				map[i] = REFITConfig.getIntProp( props, getKey( group, i + "." + name ), def );

			return map;
		}
		else if( range.length == 1 )
		{
			int[] map = new int[ number ];

			Arrays.fill( map, range[ 0 ] );

			return map;
		}
		else if( range.length < number )
		{
			throw new IllegalStateException( getKey( group, name ) );
		}
		else
		{
			return Arrays.copyOf( range, number );
		}
	}

	protected int[][] loadArrayMapping(Properties props, String group, String name, int[] def)
	{
		int[] range = REFITConfig.getIntArrayProp( props, getKey( group, name ), null );

		if( range == null )
		{
			int[][] map = new int[ number ][];

			for( int i = 0; i < number; i++ )
				map[ i ] = REFITConfig.getIntArrayProp( props, getKey( group, i + "." + name ), def );

			return map;
		}
		else if( range.length == 1 )
		{
			int[][] map = new int[ number ][];

			Arrays.fill( map, new int[] { range[ 0 ] } );

			return map;
		}
		else if( range.length < number )
		{
			throw new IllegalStateException( getKey( group, name ) );
		}
		else
		{
			int[][] map = new int[ number ][];

			for( int i = 0; i < number; i++ )
				map[ i ] = new int[] { range[ i ] };

			return map;
		}
	}
}
