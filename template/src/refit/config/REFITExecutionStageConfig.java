package refit.config;

import java.util.Properties;

public class REFITExecutionStageConfig extends REFITStageConfig
{
	private final int[][] map_to_chkptstages;

	private REFITExecutionStageConfig(Properties props, REFITCheckpointStageConfig csconf)
	{
		super( props, "exect" );
		
		map_to_chkptstages = REFITConfig.reverseIndex( csconf.getMapToExecutionStages(), getNumber() );
	}

	public static REFITExecutionStageConfig load(Properties props, REFITCheckpointStageConfig csconf)
	{
		return new REFITExecutionStageConfig( props, csconf );
	}

	public final int[] getCheckpointStageIDs(int exectid)
	{
		return map_to_chkptstages[ exectid ];
	}
}
