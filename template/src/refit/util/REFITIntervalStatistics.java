package refit.util;


public class REFITIntervalStatistics extends Thread {

	private final int interval;
	private final REFITStatisticsListener listener;
	
	private long statisticsStartTime;
	private long statisticsEndTime;
	volatile private boolean running;
	volatile private boolean logging;
	
	private int epochCounter;
	
	private int eventCounter;
	private long eventValueAggregation;
	private long eventValueMin;
	private long eventValueMax;

	private int overallEventCounter;
	private long overallEventValueAggregation;

	private TimeLog timeLog;


	public REFITIntervalStatistics(int interval, REFITStatisticsListener listener, TimeLog timeLog) {
		this.interval = interval;
		this.listener = listener;
		this.timeLog  = timeLog;

		reset();
	}
	
	
	@Override
	public void run() {
		running = true;
		logging = true;
		statisticsStartTime = System.nanoTime();
		while(running) {
			try { Thread.sleep(interval); } catch(InterruptedException ie) { }
			
			// Epoch end
			synchronized(this) {
				listener.statisticsIntervalResult(epochCounter, eventCounter, eventValueAggregation / new Float(eventCounter), eventValueMin, eventValueMax);
				epochCounter++;
	
				eventCounter = 0;
				eventValueAggregation = 0;
				eventValueMin = Long.MAX_VALUE;
				eventValueMax = Long.MIN_VALUE;
			}
		}
		statisticsEndTime = System.nanoTime();
	}

	
	public synchronized void end() {
		running = false;

		try { join(); } catch(InterruptedException ie) { }
		
		listener.statisticsOverallResult(epochCounter - 1,
				                         overallEventCounter,
				                         overallEventCounter / new Float((statisticsEndTime - statisticsStartTime) / (interval * 1000000)),
				                         overallEventValueAggregation / new Float(overallEventCounter));
	}


	public synchronized void reset()
	{
		this.eventCounter = 0;
		this.eventValueAggregation = 0;
		this.eventValueMin = Long.MAX_VALUE;
		this.eventValueMax = Long.MIN_VALUE;

		this.overallEventCounter = 0;
		this.overallEventValueAggregation = 0;

		this.epochCounter = 1;

		if( timeLog != null )
			timeLog.reset();
	}


	public synchronized void stopLogging()
	{
		logging = false;
	}


	public synchronized void event(long value) {
		if( !running )
			return;

		if( timeLog!=null && logging )
			timeLog.add( value );

		eventCounter++;
		eventValueAggregation += value;
		eventValueMin = Math.min(eventValueMin, value);
		eventValueMax = Math.max(eventValueMax, value);
		
		overallEventCounter++;
		overallEventValueAggregation += value;
		if((overallEventCounter == 100) || ((overallEventCounter % 10000) == 0)) {
			listener.statisticsProgressResult(overallEventCounter);
		}
	}
	
}
