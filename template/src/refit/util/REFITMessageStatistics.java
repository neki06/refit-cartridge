package refit.util;

import java.util.LinkedList;
import java.util.List;

import refit.config.REFITLogger;
import refit.message.REFITMessage;
import refit.message.REFITMessageContext;


public class REFITMessageStatistics {

	private final List<REFITMessageStatisticsEntry> trace;

	
	public REFITMessageStatistics() {
		this.trace = new LinkedList<REFITMessageStatisticsEntry>();
	}

	
	// ##################
	// # ACCESS METHODS #
	// ##################
	
	public static void track(REFITMessage message, String waypoint) {
		REFITMessageContext context = message.getContext();
		REFITMessageStatisticsEntry entry = new REFITMessageStatisticsEntry(waypoint);
		List<REFITMessageStatisticsEntry> trace = context.statistics.trace;
		synchronized(trace) {
			int lastElementIndex = trace.size() - 1;
			if(!trace.isEmpty() && (trace.get(lastElementIndex).waypoint.equals(waypoint))) trace.remove(lastElementIndex); 
			trace.add(entry);
		}
	}

	public static void showTrace(REFITMessageStatistics statictics) {
		String report = "";
		REFITMessageStatisticsEntry referenceEntry = null;
		synchronized(statictics.trace) {
			for(REFITMessageStatisticsEntry entry: statictics.trace) {
				if(referenceEntry == null) {
					referenceEntry = entry;
					continue;
				}
				report += "{" + entry.waypoint + ": " + ((entry.timeStamp - referenceEntry.timeStamp) / 1000000) + "} ";
			}
		}
		REFITLogger.logDebug("TRACE", report + "{END: " + ((System.nanoTime() - referenceEntry.timeStamp) / 1000000) + "}");
	}

	
	// ####################
	// # STATISTICS ENTRY #
	// ####################
	
	private static class REFITMessageStatisticsEntry {

		public final String waypoint;
		public final long timeStamp;
		
		
		public REFITMessageStatisticsEntry(String waypoint) {
			this.waypoint = waypoint;
			this.timeStamp = System.nanoTime();
		}

	}

}
