package refit.util;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class TimeLog
{
	private final long   resol;

	private final Map<Long, Integer> log = new HashMap<>();

	private       int    val_cnt;
	private       long   val_sum;
	private       long   val_min;
	private       long   val_max;

	public TimeLog(long resol)
	{
		this.resol = resol;
		
		reset();
	}


	public void reset()
	{
		val_cnt = 0;
		val_sum = 0;
		val_min = Long.MAX_VALUE;
		val_max = Long.MIN_VALUE;

		log.clear();
	}


	public void add(long value)
	{
		long    adjval = value / resol;
		Integer cnt    = log.get( adjval );
		log.put( adjval, cnt!=null ? cnt+1 : 1 );

		val_cnt++;
		val_sum += value;
		if( value < val_min ) val_min = value;
		if( value > val_max ) val_max = value;
	}


	// TODO: Return ranges instead of simple longs.
	// A time log interface could comprise:
	// void add(long value)
	// SortedMap<Range,Integer> condenseValues() | SortedSet<LogEntry> where LogEntry encapsulates ranges and value counts.
	//   Or even better: Some general type for frequency distributions.
	// getValue{Count,Sum,Min,Max,...}
	//   and some iterators for single values
	// Everything else would be implementation details of specialised time logs.
	public SortedMap<Long, Integer> condenseValues()
	{
		SortedMap<Long, Integer> condmap = new TreeMap<>();

		for( Map.Entry<Long, Integer> e : log.entrySet() )
			condmap.put( e.getKey()*resol, e.getValue() );

		return condmap;
	}


	public SummaryStatistics generateSummary()
	{
		SummaryStatistics sum = new SummaryStatistics();

		for( Map.Entry<Long, Integer> e : log.entrySet() )
		{
			double val = (e.getKey()+0.5) * resol;
			for( int j = 0; j < e.getValue(); j++ )
				sum.addValue( val );
		}

		return sum;
	}


	public int getBinCount()
	{
		return log.size();
	}


	public int  getValueCount()
	{
		return val_cnt;
	}

	public long getValueSum()
	{
		return val_sum;
	}

	public long getMinValue()
	{
		return val_min;
	}

	public long getMaxValue()
	{
		return val_max;
	}
}
