package refit.message;

import refit.replica.checkpoint.REFITCheckpointCertificate;
import refit.replica.order.REFITOrderProtocol;


public abstract class REFITInstruction extends REFITMessage {

	// #####################
	// # INSTRUCTION TYPES #
	// #####################

	public enum REFITInstructionType {
		// Client
		INVOCATION,

		// Client stage
		PANIC_NOTIFICATION,
		
		// Order stage
		VIEWCHANGE_NOTIFICATION,
		CONFIGURATION_NOTIFICATION,
		
		// Checkpoint stage
		PROGRESS_NOTIFICATION,
		PARTICAL_CHECKPOINT_NOTIFICATION,
		
		// Execution stage
		EXECUTION_PROGRESS_NOTIFICATION,

		// Hashing
		WORK_NOTIFICATION
	}

	
	// ########################
	// # ABSTRACT INSTRUCTION #
	// ########################

	public final REFITInstructionType instructionType;
	

	private REFITInstruction(REFITInstructionType instructionType) {
		super(REFITMessageType.INSTRUCTION, null, (short) -1);
		this.instructionType = instructionType;
	}

	
	@Override
	public String toString() {
		return String.format("{%-16s}", instructionType);
	}

	
	// #########################
	// # SPECIFIC INSTRUCTIONS #
	// #########################

	public static class REFITInvocationInstruction extends REFITInstruction {
		
		public final byte[] request;
		
		
		public REFITInvocationInstruction() {
			this(null);
		}
		
		public REFITInvocationInstruction(byte[] request) {
			super(REFITInstructionType.INVOCATION);
			this.request = request;
		}

	}

	public static class REFITPanicNotification extends REFITInstruction {
		
		public REFITPanicNotification() {
			super(REFITInstructionType.PANIC_NOTIFICATION);
		}
		
	}
	
	public static class REFITViewChangeNotification extends REFITInstruction {

		public REFITViewChangeNotification() {
			super(REFITInstructionType.VIEWCHANGE_NOTIFICATION);
		}

	}

	public static class REFIITConfigurationNotification extends REFITInstruction {
		
		public final REFITOrderProtocol protocol;
		
		
		public REFIITConfigurationNotification(REFITOrderProtocol protocol) {
			super(REFITInstructionType.CONFIGURATION_NOTIFICATION);
			this.protocol = protocol;
		}
		
	}
	
	public static class REFITProgressNotification extends REFITInstruction {
		
		public final long agreementSeqNr;
		public final long[] nodeProgresses;
		public final REFITCheckpointCertificate certificate;
		
		
		public REFITProgressNotification(long agreementSeqNr, long[] nodeProgresses, REFITCheckpointCertificate certificate) {
			super(REFITInstructionType.PROGRESS_NOTIFICATION);
			this.agreementSeqNr = agreementSeqNr;
			this.nodeProgresses = nodeProgresses;
			this.certificate    = certificate;
		}
		
	}

	public static class REFITPartialCheckpointNotification extends REFITInstruction {

		public final int     exectid;
		public final long    agreementSeqNr;
		public final byte[]  checkpointData;
		public final long[]  nodeProgresses;


		public REFITPartialCheckpointNotification(int exectid, long agreementSeqNr, byte[] checkpointData, long[] nodeProgresses) {
			super(REFITInstructionType.PARTICAL_CHECKPOINT_NOTIFICATION);
			this.exectid          = exectid;
			this.agreementSeqNr   = agreementSeqNr;
			this.checkpointData   = checkpointData;
			this.nodeProgresses   = nodeProgresses;
		}
	}

	public static abstract class REFITWorkNotification extends REFITInstruction {
		public REFITWorkNotification() {
			super( REFITInstructionType.WORK_NOTIFICATION );
		}

		public abstract void execute(REFITMessageAuthentication auth);
	}
}
