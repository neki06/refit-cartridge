package refit.message;

import refit.config.REFITConfig;


public class REFITUniqueID {

	public final short nodeID;
	public final long seqNr;
	
	
	public REFITUniqueID(short nodeID, long seqNr) {
		this.nodeID = nodeID;
		this.seqNr = seqNr;
	}
	
	
	@Override
	public boolean equals(Object object) {
		if(REFITConfig.ENABLE_DEBUG_CHECKS) {
			if(object == this) return true;
			if(!(object instanceof REFITUniqueID)) return false;
		}
		REFITUniqueID other = (REFITUniqueID) object;
		if(nodeID != other.nodeID) return false;
		return (seqNr == other.seqNr);
	}
	
	@Override
	public int hashCode() {
		return (int) ((((long) nodeID) << 48) + ((seqNr << 16) >> 16));
	}

	@Override
	public String toString() {
		return "[" + nodeID + "-" + seqNr + "]";
	}

}
