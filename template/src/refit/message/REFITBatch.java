package refit.message;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import refit.util.REFITPayload;


public class REFITBatch extends REFITRequest {

	private static final short BATCH_ID_MAGIC = -47;
	
	public final List<REFITRequest> requests;
	
	
	public REFITBatch(long instanceID, short from, Collection<REFITRequest> requests) {
		super(REFITMessageType.BATCH, new REFITUniqueID(BATCH_ID_MAGIC, instanceID), from, new byte[Short.SIZE >> 3], (short) -1);
		this.requests = new LinkedList<REFITRequest>();
		this.requests.addAll(requests);
	}

	public REFITBatch(ByteBuffer buffer) {
		super(buffer);
		this.requests = new LinkedList<REFITRequest>();
		short batchSize = ByteBuffer.wrap(payload).getShort();
		for(int i = 0; i < batchSize; i++) {
			REFITRequest request = new REFITRequest(buffer.slice());
			requests.add(request);
			buffer.position(buffer.position() + request.getMessageSize());
		}
		markVerified( true );
	}
	
	
	@Override
	public String toString() {
		return String.format("{%s|%d|%d|%d|%d}", type, from, uid.nodeID, uid.seqNr, requests.size());
	}

	@Override
	public boolean equals(Object object) {
		if(!super.equals(object)) return false;
		if(!(object instanceof REFITBatch)) return false;
		REFITBatch other = (REFITBatch) object;
		Iterator<REFITRequest> iterator = requests.iterator();
		Iterator<REFITRequest> otherIterator = other.requests.iterator();
		while(iterator.hasNext() && otherIterator.hasNext()) {
			if(!iterator.next().equals(otherIterator.next())) return false;
		}
		return true;
	}
	

	// #################
	// # SERIALIZATION #
	// #################

	@Override
	protected void serialize(ByteBuffer buffer) {
		ByteBuffer.wrap(payload).putShort((short) requests.size());
		super.serialize(buffer);
		for(REFITRequest request: requests) buffer.put(request.getBuffer());
	}
	
	@Override
	protected int calculatePayloadSize() {
		int size = super.calculatePayloadSize();
		for(REFITRequest request: requests) size += request.getMessageSize();
		return size;
	}

	@Override
	public byte getTypeMagic() {
		return REFITMessageType.BATCH.getMagic();
	}
	
	
	// ###########
	// # HASHING #
	// ###########

	@Override
	protected byte[] calculateHash() {
		// Prepare batch header (= request header + batch-size field)
		ByteBuffer batchHeader = getBuffer();
		batchHeader.limit(calculatePayloadSize());

		// Make sure that message hashes have been calculated
		for(REFITRequest request: requests) request.getHash();
		
		// Calculate hash
		MessageDigest digest = REFITPayload.getDigest();
		digest.update(batchHeader);
		for(REFITRequest request: requests) digest.update(request.getHash());
		return digest.digest();
	}
	
}
