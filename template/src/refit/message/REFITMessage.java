package refit.message;

import java.nio.ByteBuffer;

import refit.config.REFITConfig;
import refit.config.REFITLogger;


public abstract class REFITMessage {

	public final REFITMessageType type;
	public final REFITUniqueID uid;
	public final short from;
	
	
	protected REFITMessage(REFITMessageType type, REFITUniqueID uid, short from) {
		// This constructor is only used for messages that are created
		// on the local replica and therefore do not have to be verified.
		this( type, uid, from, true );
	}


	protected REFITMessage(REFITMessageType type, REFITUniqueID uid, short from, Boolean verified) {
		// Initialize attributes
		this.type = type;
		this.uid = uid;
		this.from = from;
		
		if( verified!=null )
			markVerified( verified );
	}
	

	@Override
	public boolean equals(Object object) {
		if(REFITConfig.ENABLE_DEBUG_CHECKS) {
			if(object == null) return false;
			if(!(object instanceof REFITMessage)) return false;
		}
		REFITMessage other = (REFITMessage) object;
		if(type != other.type) return false;
		if(!uid.equals(other.uid)) return false;
		return (from == other.from);
	}

	@Override
	public int hashCode() {
		return uid.hashCode();
	}

	@Override
	public String toString() {
		return String.format("{%s|%d|%d|%d}", type, from, uid.nodeID, uid.seqNr);
	}

	
	// ###################################
	// # SERIALIZATION & DESERIALIZATION #
	// ###################################

	// Header fields :                              size     +    type   +   uid.id   + uid.seqNr +    from
	public    static final int HEADER_SIZE   = (Integer.SIZE + Byte.SIZE + Short.SIZE + Long.SIZE + Short.SIZE) >> 3;
	protected static final int TYPE_POSITION = (Integer.SIZE >> 3);

	private transient int messageSize = -1;
	private transient int paddingSize = -1;
	private transient ByteBuffer serializationBuffer;
	

	protected REFITMessage(ByteBuffer buffer) {
		this.serializationBuffer = buffer;
		this.messageSize = buffer.getInt();
		this.type = getPrimaryType(buffer.get());
		this.uid = new REFITUniqueID(buffer.getShort(), buffer.getLong());
		this.from = buffer.getShort();
	}

	public static REFITMessage createMessage(ByteBuffer buffer) {
		REFITMessageType type = REFITMessageType.getType(buffer.get(buffer.position() + TYPE_POSITION));
		return type.createMessage(buffer);
	}

	public int getMessageSize() {
		return messageSize;
	}

	public int getPaddingSize() {
		if( paddingSize==-1 )
			paddingSize = messageSize - calculatePayloadSize();
		return paddingSize;
	}

	public ByteBuffer getBuffer() {
		if(serializationBuffer == null) {
			REFITLogger.logError(this, "getBuffer() called on a null serialization buffer");
			return null;
		}
		serializationBuffer.limit(messageSize);
		serializationBuffer.position(0);
		return serializationBuffer.slice();
	}
	
	public boolean serializeMessage(int nrOfPaddingBytes) {
		// Serialize a message only once
		if(serializationBuffer != null) return false;
		
		// Create serialization buffer
		messageSize = (calculatePayloadSize() + nrOfPaddingBytes);
		paddingSize = nrOfPaddingBytes;
		serializationBuffer = ByteBuffer.allocate(messageSize);
		
		// Serialize message
		serialize(serializationBuffer);
		return true;
	}

	public boolean hasSerialized()
	{
		return serializationBuffer!=null;
	}

	protected void setSerialized(int size, int padding, ByteBuffer buf)
	{
		messageSize = size;
		paddingSize = padding;
		serializationBuffer = buf;
	}

	protected void serialize(ByteBuffer buffer) {
		buffer.putInt(messageSize);
		buffer.put(getTypeMagic());
		buffer.putShort(uid.nodeID);
		buffer.putLong(uid.seqNr);
		buffer.putShort(from);
	}

	protected int getHeaderSize() {
		return HEADER_SIZE;
	}
	
	protected int calculatePayloadSize() {
		return HEADER_SIZE;
	}
	
	protected REFITMessageType getPrimaryType(byte magic) {
		return REFITMessageType.getType(magic);
	}

	public byte getTypeMagic() {
		return type.getMagic();
	}

	
	// ##################
	// # AUTHENTICATION #
	// ##################

	private transient Boolean isVerified;

	
	public Boolean isVerified() {
		return isVerified;
	}
	
	public void markVerified(boolean result) {
		this.isVerified = result;
	}

	
	// ###################
	// # MESSAGE CONTEXT #
	// ###################
	
	public transient REFITMessageContext context;
	
	
	public REFITMessageContext getContext() {
		if(context == null) context = new REFITMessageContext();
		return context;
	}
	
	public void mergeContexts(REFITMessageContext context) {
		this.context = context;
	}

	
	// ################
	// # MESSAGE HASH #
	// ################

	private transient byte[] hash;
	
	
	public byte[] getHash() {
		if(hash == null) hash = calculateHash();
		return hash;
	}
	
	protected byte[] calculateHash() {
		// Override in sub classes
		return null;
	}
	
	protected void setHash(byte[] hash) {
		this.hash = hash;
	}
	
}
