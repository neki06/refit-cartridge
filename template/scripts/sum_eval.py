#!/usr/bin/env python3.2

import os, datetime, csv

PATH_EVAL = "../evaluation"
PATH_SUM  = os.path.join( PATH_EVAL, "summary.txt" )


class ResultsSummary:
	@staticmethod
	def generate(resdir):
		ressum = ResultsSummary()

		ressum.date = datetime.datetime( int(name[ 0:4 ]), int(name[ 5:7 ]), int(name[ 8:10 ]),
		                              int(name[ 11:13 ]), int(name[ 14:16 ]), int(name[ 17:19 ]));

		ld = str(name).rfind( "-" )
		ressum.config   = name[ 30:ld ]
		ressum.nclients = int(name[ ld+1: ])

		rescli = os.path.join( resdir, "client0.log" )
		with open( rescli, 'rt', newline='', encoding='utf-8' ) as resfile:
			dialect = csv.Sniffer().sniff( resfile.read( 1024 ) )
			resfile.seek( 0 )

			for data in csv.DictReader( resfile, dialect=dialect ):
				ressum.reqcount   = int(data[ "cnt" ])
				ressum.time       = int(data[ "time" ])
				ressum.sumlat     = int(data[ "sum" ])
				ressum.minlat     = int(data[ "min" ])
				ressum.maxlat     = int(data[ "max" ])
				ressum.avglat     = int(data[ "mean" ])
				ressum.astddevlat = float(data[ "approxstddev" ])
				ressum.reqpersec  = int(data[ "rps" ])
				break

		rescpu = os.path.join( resdir, "resources-cpu.log" )
		with open( rescpu, 'rt', newline='', encoding='utf-8' ) as resfile:
			data = list( csv.DictReader( resfile, [None, None, None, None, "cpu", None, None, None ], dialect=csv.excel_tab ) )

			if 95 < len(data) < 110:
				cpu = 0
				for l in data[65:85]:
					cpu = cpu + float(l[ "cpu" ])
				ressum.avgcpu = cpu / 20

		return ressum


fields = ["Date", "Type", "Config", "Cores", "Clients", "Throughput", "Latency", "Min", "Max", "AStdDev", "AvgCPU" ]

with open( PATH_SUM, "wt", newline="", encoding="utf-8" ) as sumfile:
	writer = csv.DictWriter( sumfile, fields )

	writer.writeheader()

	for name in os.listdir( PATH_EVAL ):
		resdir = os.path.join( PATH_EVAL, name )

		if not os.path.isdir( resdir ):
			continue

		ressum = ResultsSummary.generate( resdir )

		writer.writerow( { "Date": ressum.date, "Config": ressum.config, "Clients": ressum.nclients,
		                   "Throughput": ressum.reqpersec, "Latency": ressum.avglat, "Min": ressum.minlat,
		                   "Max": ressum.maxlat, "AStdDev": ressum.astddevlat, "AvgCPU": ressum.avgcpu } )
